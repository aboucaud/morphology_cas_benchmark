CAS
===

Installation requirements
-------------------------

* IRAF / PyRAF

Documentation about IRAF cl scripts in general : http://iraf.noao.edu/docs/prog.html

(see Elie's instructions at the bottom)

Instructions from Chris
-----------------------

### `sort4.cl` and `ngcas.cl`

> Instructions - Basically you have to first run `sort4.cl` which is a code that takes a list of ra, dec and produces cutouts for each galaxy, it's corresponding segmentation maps, and weight map.   If you don't have a weight map simply using a image with the same size as the main image but with all pixel values equal to one.

> These images are then fed into the task `ngcas.cl` --- using the input file `input.cas` - I'm attaching an example using your images.

> More details:

> 1) To use `sort4.cl` -- create a file called 'list' which is the list of the images which contain the images where the galaxies you want to study are located.  List has the format

> image_name seg_name wht_name

> image_name is the name with the image (drz usually for hst), seg_name is the corresponding segmentation map and wht_name is the corresponding weight map.
But if you have more images (which you will) then you can add a line for each image.   For example, if you are analyzing the CANDELS fields you would have a line for each tile.

> Also used by `sort4.cl` is a file you create which is a list of galaxies you want to analyse.  The format is id, ra, dec, and other information.  ra and dec should be in decimal notation.

> To run this, you have to use iraf.  To do so:
```
cl> task $ sort4 = sort4.cl
cl> sort4
```
and it should run.

> the output will be a file called input.cas which goes into the main CAS code.  It also produces cutout postage stamps for each galaxy in the list and saves it into the directory you are working in.

> 2) load packages needed to run CAS by typing into the iraf terminal the following, all as single lines: noao, digiphot, apphot, artdata, tables, tools

> 3) load up ngcas
```
cl> task $ ngcas = ngcas.cl
cl> ngcas
```
then when it asks for the input file, type 'input.cas', then let it go. It should run through and produce a file given by the name you specify when you run the code.  The output file has the following:

> `#  Name  x        y      C    E(C)    A    E(A)   S   E(S)  M_20 Gini Petr   HalfR  Flux   inflag casflag`

> x,y is the centre, C,A,S are the CAS parameters with their errors (E), M_20, Gini are two other parameters, HalfR is the half light radius as measured through the Petrosian radius (see Conselice et al. 2000) for an explanation of this.


### `cas.cl`

> Using this version of CAS is fairly simple.  One thing that is essential is that there are no background/foreground galaxies/stars near the galaxy of interest. Looks like this will be an issue for your objects as there seems to be many of them around.  One way to do this is to use the iraf task 'imedit', another way is to use sextractor to detect the objects and then to write a code to replace them with 'sky' -- i.e., you cannot just mask them, but have to actually replace with a copy of the sky.  This can be done by using the average sky value with the correct sigma implemented as well.  Your best bet is probably to use imedit and the 'a' command which will do this in an interactive way.

> Some quick instructions:

> 1. Save the file cas.cl into a directory.

> 2. Create an input file with the format:
     `image_name  x   y   x1  x2  y1  y2`
     one line per object, for each object
     where
     * image_name = the name of the image, which must be a fits file
     * x and y is the center of the galaxy
     * x1 x2 y1 y2 is the back ground area, which should be about 20-30 pixels in
width and contains only the sky.

> 3. Make sure your imtype = fits in your IRAF login.cl file

> 4. Before running copy these commands into the command line:
    ```
    flpr
    flpr
    noao
    digiphot
    apphot
    artdata
    tables
    ttools
    ```

> 5. type
    ```
    cl> task $ cas = cas.cl
    cl> cas
    ```

> 6. The output file will be:
    `ID, C, CE, A, AE, S, SE, bigrad, halfr, flux, sflag`
    where:
    * ID: your ID for the object in galaxies.in
    * C,CE
    * A,AE - The CAS values and their errors
    * S,SE
    * bigrad - the Petrosian radius
    * halfr - the half-light radius
    * flux - flux within the Petrosian radius
    * sflag - a flag revealing if edge was hit, > 1 if so

Additional instructions from Elie
---------------------------------

To start the iraf environment, enter the following command at unix prompt :
```
cl
```

Package STSDAS (stsci_iraf-3.17, containing imgtools) has also been installed from 
http://www.stsci.edu/institute/software_hardware/stsdas/download-stsdas


For sort4.cl, some additional keywords are needed in the input fits file.
Otherwise, you get the following error when running sort4 :
```
ERROR: Image header parameter not found (CD1_2)
  "rd2xy (image, ra, dec, hour=no, >> "out")"
     line 59: sort4.cl
     called as: `sort4 ()'
```
To verify that they are missing (in this case, the fits is nonoise_vis_8b-sc.fits) and add the CD1_2 and CD2_1 keywords, you can use the following:
```
imgtools> imheader nonoise_vis_8b-sc.fits longheader+
nonoise_vis_8b-sc.fits[7701,7666][real]: 
No bad pixels, min=0., max=0. (old)
Line storage mode, physdim [7701,7666], length of user area 648 s.u.
Created Thu 11:36:10 17-Mar-2016, Last modified Thu 11:36:10 17-Mar-2016
Pixel file "nonoise_vis_8b-sc.fits" [ok]
EXTEND  =                    T / This file may contain FITS extensions
COMMENT  Created by SkyMaker version 3.3.3 (2015-05-25)
BSCALE  =               1.0000 / True value = BSCALE*PIXEL+BZERO
BZERO   =               0.0000 / True value = BSCALE*PIXEL+BZERO
CDELT1  =     -2.777777778E-05
CDELT2  =      2.777777778E-05
CTYPE1  = 'RA---TAN'
CTYPE2  = 'DEC--TAN'
EQUINOX =                 2000
CRPIX1  =                  1.0
CRPIX2  =                  1.0
CRVAL1  =          53.68188301
CRVAL2  =         -27.28559177
CD1_1   =   -2.77777777778E-05
CD2_2   =    2.77777777778E-05
TOTFLUX =                  1.0

imgtools> hedit nonoise_vis_8b-sc.fits CD1_2 0.0 add+
add nonoise_vis_8b-sc.fits,CD1_2 = 0.
update nonoise_vis_8b-sc.fits ? (yes): yes
nonoise_vis_8b-sc.fits updated
imgtools> hedit nonoise_vis_8b-sc.fits CD2_1 0.0 add+
add nonoise_vis_8b-sc.fits,CD2_1 = 0.
update nonoise_vis_8b-sc.fits ? (yes): yes
nonoise_vis_8b-sc.fits updated
imgtools> imheader nonoise_vis_8b-sc.fits longheader+ | grep CD
CDELT1  =     -2.777777778E-05
CDELT2  =      2.777777778E-05
CD1_1   =   -2.77777777778E-05
CD2_2   =    2.77777777778E-05
CD1_2   =                   0.
CD2_1   =                   0.
```

The sort4.cl can then be run (it takes 2 input files, a "list" file, and a "cata" file containing 
indexes, ra, dec, other unused information if wanted)
This creates the files input.cas, catalog.cas as well as all image cutouts
(for example, the 119th cutout triplet is {u.118.1.fits, u.seg.118.1.fits, u.wht.118.1.fits})

The ngcas.cl script can then be run (user input is requested):
```
imgtools> ngcas
Multiple Object CAS -- Input file: id, image,  wmap_image, segmap, x,y: input.cas
Outfile File Name: ngcas_out     
0.  3.97 0.156 0.095 0.007 0.13 0.001 -2.381 0.681 32.064 8.437 259.139 0. 1 
1.  1.949 0.359 0.017 0. 0. 0. 0. 0.76 6.087 1.972 19.925 0. 7 
2.  3.968 0.243 0.212 0.007 0.36 0.002 -2.279 0.664 18.371 3.523 46.906 0. 1 
...
```