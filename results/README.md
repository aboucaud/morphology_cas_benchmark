Test results
============

Small simulated image (from Emiliano)
-------------------------------------

See [notebook](http://nbviewer.ipython.org/urls/git.ias.u-psud.fr/aboucaud/morphology_cas_benchmark/raw/master/results/notebook/test_on_merged_catalogs.ipynb).


Glueviz figures
---------------

In order to compare the 2 available methods (NGCAS versus GalSVM),
a small region of the whole image has been analyzed with glueviz (interactive tool for data analysis).

The VIS cutout is shown in this image :
![VIS cutout](https://git.ias.u-psud.fr/aboucaud/morphology_cas_benchmark/raw/master/results/figs/vis_cutout.png)

The merged catalog for this region is represented in (ra,dec) hereunder:
![cutout in catalog](https://git.ias.u-psud.fr/aboucaud/morphology_cas_benchmark/raw/master/results/figs/vis_cutout_subsets.png)

The different subsets were build in glueviz with the names shown hereunder (consider this image as the legend for all other figures) :
![subsets_definition](https://git.ias.u-psud.fr/aboucaud/morphology_cas_benchmark/raw/master/results/figs/subsets_definition.png)

Help for the legend:
* a_99 is the subset of points in the merged catalog where a_GalSVM = 99
* a_not_99_ra_dec is the subset of points in the selected region where the iraf concentration > 2 and a_GalSVM is not 99 (undefined)
* c_iraf is divided in 2 subsets : one region around 1.5 (red +) and one region around 2.0 (orange stars)

The subsets are also shown in the following images :
![concentration NGCAS versus GalSVM](https://git.ias.u-psud.fr/aboucaud/morphology_cas_benchmark/raw/master/results/figs/vis_cutout_subset_concentration.png)

Comparison of ngcas results with and without noise in input image:

![comparison with and without noise](https://git.ias.u-psud.fr/aboucaud/morphology_cas_benchmark/raw/master/results/figs/noise_versus_no_noise_iraf_ngcas.png)
