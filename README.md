Morphology code benchmark
=========================

General information
-------------------

### Parameters to be tested

  * CAS (Concentration - Asymmetry - Smoothness)
  * \\(M_{20}\\)
  * Gini coefficient

### Testing material

  * DC2 image  + SExtractor segmentation map ([download link][dc2], 7.4 Go)
  * Euclid simulated dataset from Emiliano Merlin
     * 0.5 sq.deg ([download link][smallsim], 1.2 Go)
     * 4 sq.deg ([download link][bigsim], 8.2 Go)
  * HST images + SExtractor segmentation map

### Codes to be tested

| Codes                       | Custodian              | Language |
| --------------------------- | ---------------------- | -------- |
| [galSVM](galSVM/README.md)  | [Marc Huertas][mhc]    |    IDL   |
| [CAS](conselice/README.md)  | [Chris Conselice][cc]  |   IRAF   |


Results
-------

Results can be seen on [this](results/README.md) page.

=> The main notebook of interest is [here][notebook].

[dc2]: ftp://ftp.ias.u-psud.fr/aboucaud/pub/sim_vis_DC2.tar.gz
[smallsim]: ftp://ftp.ias.u-psud.fr/aboucaud/pub/EGGSIM8B.tar.gz
[bigsim]: ftp://ftp.ias.u-psud.fr/aboucaud/pub/EUCLIDSIM_4SqDeg.tar.gz

[notebook]: http://nbviewer.ipython.org/urls/git.ias.u-psud.fr/aboucaud/morphology_cas_benchmark/raw/master/results/notebook/stat_study_morph_codes.ipynb

[mhc]: mailto:marc.huertas@obspm.fr
[cc]: mailto:conselice@nottingham.ac.uk
