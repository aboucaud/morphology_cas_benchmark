PRO galSVM,FILE=filename,flag

;*******************************************************************************
;
;+
;NAME:
;    galSVM
;
;CATEGORY :
;       Astronomy
;
;SYNTAX:
;      galSVM,flag, FILE=filename
;        
;      
;
;DESCRIPTION :
; 
;Main galSVM file. Does everything.

;Executes galSVM with the settings specified in the configuration file
;"filename". By default it looks for a configuration file called
;galSVM.config in the current directory.
;
;   galSVM,'all' : does everything in one single run.
;
;    galSVM, 'prepare': runs sextractor on the real images. just run
;this step if you hav not already run sextractor manually
;
;   galSVM, 'simulate': simulates stamps at high z
;
;   galSVM, 'drop' : drops simulated galaxies in real bg  
;
;   galSVM, 'mophology': computes morpho. parameters of real and mock
;   galaxies
; 
;   galSVM, 'morphology_mock': computes morpho. parameters just for mock galaxies 
;   galSVM, 'morphology_real': computes morpho. parameters just form
;   real galaxies 
;
;   galSVM, 'classify': runs SVM on the real catalog using the mock as training
;
;   galSVM, 'clean'  : removes all folders called galSVM_temp*** from
;   previous runs of galSVM 
;
;
;  The code creates at each run a folder called galSVM_temp*** and
;  puts all files used for the run.
;
; 
;
;  MANDATORY ARGUMENTS:
;
;    flag                : (input) string that says what to do
;
;
;  OPTIONAL ARGUMENTS:
;
;
;    FILE                 : (input) name of configuration file. By
;    default it looks for a file called galSVM.config in the current folder.         
;
;
;AUTHOR:
;    $Author: huertas-company $
;-



IF flag NE 'all' AND flag NE 'simulate' AND flag NE 'drop' AND flag NE 'morphology' AND flag NE 'morphology_mock' AND flag NE 'morphology_real' AND flag NE 'classify' AND flag NE 'clear' AND flag NE 'help' AND flag NE 'prepare' THEN BEGIN
message, 'Unkown option '+flag+'. Please use one of the following options: all, prepare, simulate, drop, morphology, morphology_mock, morphology_real, classify, clear.'
ENDIF

IF NOT KEYWORD_SET(filename) THEN filename='galSVM.config'

config=galSVM_read_config_file(FILE=filename)

IF tag_exist(config,'dummy') EQ 1 THEN GOTO, OUT


IF flag EQ 'clear' THEN GOTO,CLEAR

IF flag EQ 'help' THEN BEGIN

print,'******************************************************************************************************'
print,'******************************************************************************************************'
print,'SYNTAX: galSVM,flag,FILE=filename'
 print, ''
print,'Executes galSVM with the settings specified in the configuration file filename. By default it looks for a configuration file called galSVM.config in the current directory.'
print, ''
print, 'galSVM,all : does everything in one single run.'
print, ''
print, 'galSVM, simulate: simulates stamps at high z'
print, ''
print,'galSVM, drop : drops simulated galaxies in real bg  '
print, ''
print,'galSVM, mophology: computes morpho. parameters of real and mock galaxies'
 print, ''
print, 'galSVM, morphology_mock: computes morpho. parameters just for mock galaxies '
print, ''  
print  , 'galSVM, morphology_real: computes morpho. parameters just form real galaxies '
print, ''
print , 'galSVM, classify: runs SVM on the real catalog using the mock as training'
print, ''
print, 'galSVM, clean  : removes all folders called galSVM_temp*** from  previous runs of galSVM '
print, ''

print, ' The code creates in every run a folder called galSVM_temp*** and puts all files used for the run.'

print,'*******************************************************************************************************'
print,'********************************************************************************************************'

GOTO,OUT
ENDIF



lz_filters_names=strsplit(config.lz_filters_names,',',/EXT)
lz_filters_lambdas=strsplit(config.lz_filters_lambdas,',',/EXT)
lz_filters_fwhms=strsplit(config.lz_filters_fwhms,',',/EXT)
lz_filters_pix_scale=strsplit(config.lz_filters_pix_scale,',',/EXT)
lz_filters_sigma=strsplit(config.lz_filters_sigma,',',/EXT)
lz_filters_zerop=strsplit(config.lz_filters_zerop,',',/EXT)

hz_filters_names=strsplit(config.hz_filters_names,',',/EXT)
hz_filters_lambdas=strsplit(config.hz_filters_lambdas,',',/EXT)
hz_filters_fwhms=strsplit(config.hz_filters_fwhms,',',/EXT)
hz_filters_pix_scale=strsplit(config.hz_filters_pix_scale,',',/EXT)
hz_filters_sigma=strsplit(config.hz_filters_sigma,',',/EXT)
hz_filters_zerop=strsplit(config.hz_filters_zerop,',',/EXT)
hz_filters_gain=strsplit(config.hz_filters_gain,',',/EXT)
hz_filters_satur_level=strsplit(config.hz_filters_satur_level,',',/EXT)

mock_sel_param=strsplit(config.MOCK_SEL_PARAM,',',/EXT)
mock_min=strsplit(config.MOCK_MIN,',',/EXT)
mock_max=strsplit(config.MOCK_MAX,',',/EXT)
libsvm_param=strsplit(config.LIBSVM_PARAMETERS,',',/EXT)
libsvm_param_min=strsplit(config.LIBSVM_PARAM_MIN,',',/EXT)
libsvm_param_max=strsplit(config.LIBSVM_PARAM_MAX,',',/EXT)
libsvm_bounds=fltarr(2,n_elements(libsvm_param))
FOR i=0,n_elements(libsvm_param)-1 DO BEGIN
libsvm_bounds[0,i]=float(libsvm_param_min[i])
libsvm_bounds[1,i]=float(libsvm_param_max[i])
ENDFOR

mock_min=float(mock_min)
mock_max=float(mock_max)

sel_param=strsplit(config.SEL_PARAM,',',/EXT)
sel_param_min=strsplit(config.SEL_PARAM_MIN,',',/EXT)
sel_param_max=strsplit(config.SEL_PARAM_MAX,',',/EXT)

sel_param_min=float(sel_param_min)
sel_param_max=float(sel_param_max)



libsvm_morpho_bounds=strsplit(config.LIBSVM_MORPHO_BORDERS,',',/EXT)
libsvm_morpho_bounds=float(libsvm_morpho_bounds)


lz_filters_lambdas=float(lz_filters_lambdas)
lz_filters_fwhms=float(lz_filters_fwhms)
lz_filters_pix_scale=float(lz_filters_pix_scale)
lz_filters_sigma=float(lz_filters_sigma)
lz_filters_zerop=float(lz_filters_zerop)

hz_filters_lambdas=float(hz_filters_lambdas)
hz_filters_fwhms=float(hz_filters_fwhms)
hz_filters_pix_scale=float(hz_filters_pix_scale)
hz_filters_sigma=float(hz_filters_sigma)
hz_filters_zerop=float(hz_filters_zerop)
hz_filters_gain=float(hz_filters_gain)
hz_filters_satur_level=float(hz_filters_satur_level)


psf_flag=float(config.PSF)
mc_flag=float(config.MONTECARLO)
max_objects=float(config.MAX_OBJECTS)
IF config.seed EQ 0 THEN seed=randomu(s)*float(SYSTIME(1,/SECONDS)) ELSE seed=config.seed
multi_lambda=float(config.MULTI_LAMBDA_FLAG)
print,'SEED:', seed[0]


IF float(config.RUN_SEX_FIRST) EQ 1 AND (flag EQ 'prepare' OR flag EQ 'all') THEN BEGIN 

SPAWN,strcompress(config.sex_command+' '+strcompress(config.real_image_path+config.field_image,/REM)+ ' -c '+config.sex_file+ ' -CATALOG_NAME '+strcompress(config.real_catalog_path+config.real_catalog,/remove_all)+ ' -CHECKIMAGE_NAME '+strcompress(config.real_segmentation_image_path+config.segmentation_image,/remove_all)+' -MAG_ZEROPOINT '+string(config.hz_filters_zerop)+' -GAIN '+string(config.hz_filters_gain)+' -PIXEL_SCALE '+string(config.hz_filters_pix_scale)+' -SEEING_FWHM '+string(config.hz_filters_fwhms)+' -SATUR_LEVEL '+string(config.hz_filters_satur_level))

SPAWN,strcompress(config.sex_command+' '+strcompress(config.output_path+config.back_image,/REM)+ ' -c '+config.sex_file+ ' -CATALOG_NAME '+strcompress(config.output_path+'back_cat.fit',/remove_all)+ ' -CHECKIMAGE_NAME '+strcompress(config.output_path+config.seg_back_image,/remove_all)+' -MAG_ZEROPOINT '+string(config.hz_filters_zerop)+' -GAIN '+string(config.hz_filters_gain)+' -PIXEL_SCALE '+string(config.hz_filters_pix_scale)+' -SEEING_FWHM '+string(config.hz_filters_fwhms)+' -SATUR_LEVEL '+string(config.hz_filters_satur_level))


ENDIF

;read stuff

cd, config.local_catalog_path
local_catalog=mrdfits(config.local_catalog,float(config.loc_cat_ext))
stop
d;sjddsl
cd, config.real_catalog_path
hz_catalog=mrdfits(config.real_catalog,float(config.cat_ext))
readcol,config.Z_HISTO,z,Nz
z_table=fltarr(n_elements(z),2)
z_table[*,0]=z
z_table[*,1]=Nz
readcol,config.MAG_HISTO,mag,Nm
mag_table=fltarr(n_elements(mag),2)
mag_table[*,0]=mag
mag_table[*,1]=Nm

cd, config.real_image_path
hz_image=readfits(config.field_image,EXTEN_NO=float(config.FIM_EXT))
cd, config.REAL_SEGMENTATION_IMAGE_PATH

hz_seg=readfits(config.segmentation_image,EXTEN_NO=float(config.SIM_EXT))




cd, config.output_path
IF flag  EQ 'all' OR flag EQ 'simulate' THEN BEGIN
cd, config.output_path
galsvm_dir=file_search('galSVM_temp*')
IF galsvm_dir[0] EQ '' THEN galsvm_trash=strcompress(config.output_path+'galSVM_temp001'+'/',/REM)
IF galsvm_dir[0] NE '' THEN galsvm_trash=strcompress(config.output_path+'galSVM_temp'+string(n_elements(galsvm_dir)+1,FOR='(I03)')+'/',/REM)
SPAWN,'mkdir '+galsvm_trash
ENDIF ELSE BEGIN
cd, config.output_path
galsvm_dir=file_search('galSVM_temp*')
galsvm_trash=strcompress(config.output_path+galsvm_dir[n_elements(galsvm_dir)-1]+'/',/REM)
ENDELSE

back_image=readfits(config.BACK_IMAGE)
seg_back_image=readfits(config.SEG_BACK_IMAGE)

IF multi_lambda EQ 1 THEN BEGIN

IF flag EQ 'all' OR flag EQ 'simulate' THEN BEGIN
local_filters={filters,names:lz_filters_names,lambdas:lz_filters_lambdas,fwhms:lz_filters_fwhms,pix_scale:lz_filters_pix_scale,sigma:lz_filters_sigma,zerop:lz_filters_zerop}

IF config.PSF EQ 0 AND config.DIMMING EQ 0 THEN galSVM_adapt_catalog,seed[0],config.local_catalog_path,galsvm_trash,local_catalog,config.nobjects,hz_filters_pix_scale[0],lz_filters_pix_scale,hz_filters_zerop[0],hz_filters_fwhms[0],lz_filters_fwhms,hz_filters_sigma[0],"simucat.fit",Z_HISTO=z_table,MAG_HISTO=mag_table,MULTI_BAND=local_filters,LAMBDA_OBS=hz_filters_lambdas[0]

IF config.PSF EQ 1 AND config.DIMMING EQ 0 THEN galSVM_adapt_catalog,seed[0],config.local_catalog_path,galsvm_trash,local_catalog,config.nobjects,hz_filters_pix_scale[0],lz_filters_pix_scale,hz_filters_zerop[0],hz_filters_fwhms[0],lz_filters_fwhms,hz_filters_sigma[0],"simucat.fit",Z_HISTO=z_table,MAG_HISTO=mag_table,MULTI_BAND=local_filters,LAMBDA_OBS=hz_filters_lambdas[0],/PSF


IF config.PSF EQ 0 AND config.DIMMING EQ 1 THEN galSVM_adapt_catalog,seed[0],config.local_catalog_path,galsvm_trash,local_catalog,config.nobjects,hz_filters_pix_scale[0],lz_filters_pix_scale,hz_filters_zerop[0],hz_filters_fwhms[0],lz_filters_fwhms,hz_filters_sigma[0],"simucat.fit",Z_HISTO=z_table,MAG_HISTO=mag_table,MULTI_BAND=local_filters,LAMBDA_OBS=hz_filters_lambdas[0],/DIMMING

IF config.PSF EQ 1 AND config.DIMMING EQ 1 THEN galSVM_adapt_catalog,seed[0],config.local_catalog_path,galsvm_trash,local_catalog,config.nobjects,hz_filters_pix_scale[0],lz_filters_pix_scale,hz_filters_zerop[0],hz_filters_fwhms[0],lz_filters_fwhms,hz_filters_sigma[0],"simucat.fit",Z_HISTO=z_table,MAG_HISTO=mag_table,MULTI_BAND=local_filters,LAMBDA_OBS=hz_filters_lambdas[0],/PSF,/DIMMING


cd, config.output_path
IF flag NE 'all' THEN GOTO,OUT

ENDIF




IF flag EQ 'all' OR flag EQ 'drop' THEN BEGIN

galSVM_drop_galaxies,seed[0],galsvm_trash,"simucat.fit",back_image,$
seg_back_image,max_objects,hz_filters_zerop[0],hz_filters_gain[0],hz_filters_pix_scale[0],hz_filters_FWHMS[0],hz_filters_satur_level[0],"simul_","simucat.fit",config.sex_command,config.sex_file

cd, galsvm_trash
SPAWN,"source sex_khz.sh"
cd, config.output_path
IF flag NE 'all' THEN GOTO,OUT
ENDIF



IF flag EQ 'all' OR flag EQ 'morphology_mock' OR flag EQ 'morphology' THEN BEGIN

galSVM_mock_morpho_catalog,galsvm_trash,"simul_","simucat.fit",hz_filters_pix_scale[0],hz_filters_zerop[0],"simucat.fit"
cd, config.output_path
IF flag NE 'all' AND flag NE 'morphology' THEN GOTO,OUT
ENDIF


IF flag EQ 'all' OR flag EQ 'morphology_real' OR flag EQ 'morphology' THEN BEGIN

IF sel_param[0] EQ 'NONE' THEN BEGIN
wpos=indgen(n_elements(hz_catalog.NUMBER))
GOTO,morphology_real
ENDIF

FOR i=0,n_elements(sel_param)-1 DO BEGIN
IF i EQ 0 THEN wpos=where_tag(hz_catalog,TAG_NAME=sel_param[i],RANGE=[sel_param_min[i],sel_param_max[i]]) ELSE wpos=where_tag(hz_catalog,TAG_NAME=sel_param[i],RANGE=[sel_param_min[i],sel_param_max[i]],ISELECT=wpos)
ENDFOR



IF wpos[0] EQ -1 THEN BEGIN
print,'No objects were selected. Please select at least one object to classify. Stopping...'
GOTO,OUT
ENDIF

morphology_real:
galSVM_real_morpho_catalog,hz_catalog,hz_image,wpos,hz_seg,hz_filters_pix_scale[0],hz_filters_zerop[0],config.output_path,config.output_catalog
cd, config.output_path
IF flag NE 'all' THEN GOTO,OUT
ENDIF




IF flag EQ 'all' OR flag EQ 'classify' THEN BEGIN



FOR i=0,config.MONTECARLO[0]-1 DO BEGIN
k=0
cd, galsvm_trash
simucat=mrdfits("simucat.fit",1)
pos_train=indgen(n_elements(simucat.INDEX))

;select objects to be considered from the training catalog
FOR l=0,n_elements(mock_sel_param)-1 DO BEGIN
pos_train=where_tag(simucat,TAG_NAME=mock_sel_param[l],RANGE=[mock_min[l],mock_max[l]],ISELECT=pos_train)
ENDFOR



shift_number=round(randomu(dummy_var))*n_elements(simucat.INDEX)

WHILE k LT n_elements(libsvm_morpho_bounds) DO BEGIN
pos_train_shift=shift(pos_train,shift_number)
pos=clean_morpho(simucat,libsvm_param,libsvm_bounds,pos_train_shift[0:config.ntrain-1])
pos2=eq_training_sample(simucat,'MORPHO',libsvm_morpho_bounds[k:k+2],pos,FRAC=.5)
pos_test=clean_morpho(simucat,libsvm_param,libsvm_bounds,pos_train_shift[config.ntrain:n_elements(pos_train)-1])

cd, galsvm_trash

IF i EQ 0 THEN SPAWN,"mkdir libsvm"
cd, "libsvm"
print, "NUMBER OF OBJECTS USED FOR TRAINING IN CLASS "+string(k)+": "+string(n_elements(pos2))
libSVM_write,simucat,libsvm_param,pos2,CLASS_POS='MORPHO',CLASS_BORDERS=libsvm_morpho_bounds[k:k+2],/TWOCLASS,"train"
libSVM_write,simucat,libsvm_param,pos_test,CLASS_POS='MORPHO',CLASS_BORDERS=libsvm_morpho_bounds[k:k+2],/TWOCLASS,"test"
SPAWN,strcompress("python "+strcompress(config.libsvm_path+"/tools/easy.py",/REM)+" "+"train"+" "+"test"),/SH
number_mc=string(i,FOR='(I04)')
IF i NE 0 THEN test_cat=mrdfits("test.fit",1) ELSE test_cat=simucat
libSVM_extract_output,test_cat,"test.predict",pos_test,strcompress('PROBA'+string(k/3+1)+'_MC'+string(i),/REM),"test.fit"


cd, config.output_path
output_cat=mrdfits(config.output_catalog,1)
IF i EQ 0 THEN pos_real=clean_morpho(output_cat,libsvm_param,libsvm_bounds,indgen(n_elements(output_cat.NUMBER)))
print, 'POS_REAL:',pos_real
cd, strcompress(galsvm_trash+'/libsvm',/REM)
IF i EQ 0 THEN libSVM_write,output_cat,libsvm_param,pos_real,/TWOCLASS,"real"
SPAWN,strcompress("python "+strcompress(config.libsvm_path+"/tools/easy.py",/REM)+" "+"train"+" "+"real"),/SH
libSVM_extract_output,output_cat,"real.predict",pos_real,strcompress('PROBA'+string(k/3+1)+'_MC'+string(i),/REM),config.output_catalog
SPAWN, "mv "+config.output_catalog+" "+config.output_path
k=k+3
ENDWHILE
ENDFOR



;add the average probability and variance to output catalog
cd, config.output_path
output_cat=mrdfits(config.output_catalog,1)
k=0
WHILE k LT n_elements(libsvm_morpho_bounds) DO BEGIN 
tags=tag_names(output_cat)
match_pos=where(strmatch(tags,strcompress('PROBA'+string(k/3+1)+'_MC*',/REM)) EQ 1)
table=fltarr(n_elements(output_cat.NUMBER),n_elements(match_pos))
FOR j=0,n_elements(match_pos)-1 DO BEGIN
table[*,j]=output_cat.(match_pos[j])
ENDFOR

mean_proba=total(table,2)/n_elements(match_pos)
err_proba=mean_proba*0.
FOR j=0,n_elements(output_cat.NUMBER)-1 DO BEGIN
err_proba[j]=sqrt(variance(table[j,*]))
ENDFOR

IF tag_exist(output_cat,strcompress('PROBA'+string(k/3+1)+'_AVG',/REM)) EQ 1 THEN BEGIN 
;HELP, output_cat,/struct
;HELP, new_output_cat,/struct
cat_temp=output_cat
REMOVE_TAGS,cat_temp,[strcompress('PROBA'+string(k/3+1)+'_AVG',/REM),strcompress('PROBA'+string(k/3+1)+'_ERR',/REM)],new_output_cat
output_cat=new_output_cat
ENDIF
output_cat=JJADD_TAG(output_cat,strcompress('PROBA'+string(k/3+1)+'_AVG',/REM),mean_proba)
output_cat=JJADD_TAG(output_cat,strcompress('PROBA'+string(k/3+1)+'_ERR',/REM),err_proba)
k=k+3
ENDWHILE

mwrfits,output_cat,config.output_catalog[0],/CREATE

ENDIF
ENDIF













CLEAR:

IF flag EQ 'clear' THEN BEGIN

cd, config.output_path
IF (file_search('galSVM_temp*'))[0] NE '' THEN BEGIN 
SPAWN,'rm -r galSVM_temp*'
print, '------------------------------------------------------------------------'
print, 'All galSVM_temp* directories have been removed from '+config.output_path
print, '------------------------------------------------------------------------'
ENDIF ELSE BEGIN
print, '-----------------------------------------------'
print, 'Nothing to remove'
print, '-----------------------------------------------'
ENDELSE

ENDIF

OUT:
END



;Copyright 2011 Marc Huertas-Company

;This file is part of GALSVM.

;    GALSVM is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 3 of the License, or
;    (at your option) any later version.

;    GALSVM is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.

 ;   You should have received a copy of the GNU General Public License
 ;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
