
PRO libSVM_extract_output,table_cat,libsvm_out,ind,tag_names,file_out

;+
;NAME:
;    libSVM_extract_output_real
;
;CATEGORY :
;       Astronomy
;
;SYNTAX:
;       libSVM_extract_output_real, table_cat, libsvm_out, file_out, header 
;        
;      
;
;DESCRIPTION :
; 
;Extracts the morphological classification from the libSVM output
;
; 
;
;  MANDATORY ARGUMENTS:
;    table_cat           : (input) catalog structure
;
;    libsvm_out          : (input) LibSVM output
;
;    ind                 : (input) indices to be written 
;
;    tag_names           : (input) names of tags to be added
;
;    file_out            : (output) Output catalog 
;
;
;
;AUTHOR:
;    $Author: huertas-company $
;-



size_x=(size(table_cat.NUMBER))[1]

morphov=fltarr(size_x)+99.9
proba1v=morphov
proba2v=morphov


readcol,libsvm_out,morpho1,morpho2,FOR='X,F,F',/SILENT

param_to_add=fltarr(size_x)+99.9
IF morpho1[0] EQ -1 THEN param_to_add[ind]=morpho1[1:n_elements(morpho1)-1]
IF morpho2[0] EQ -1 THEN param_to_add[ind]=morpho2[1:n_elements(morpho2)-1]

IF tag_exist(table_cat,tag_names) EQ 1 THEN BEGIN 
REMOVE_TAGS,table_cat,tag_names,new_table_cat
table_cat=new_table_cat
ENDIF
table_cat=JJADD_TAG(table_cat,tag_names,param_to_add)


mwrfits,table_cat,strcompress(file_out,/rem),/CREATE


END


PRO libSVM_write,morpho_table,par_vec_pos,indices,CLASS_POS=class_pos,CLASS_BORDERS=class_borders,TWOCLASS=twoclass,filename

;+
;NAME:
;    libSVM_WRITE
;
;CATEGORY :
;       Astronomy
;
;SYNTAX:
;       libSVM_write, morpho_table, par_vec_pos, filename, CLASS_POS=class_pos, CLASS_BORDERS=class_borders 
;        
;      
;
;DESCRIPTION :
; 
;Writes an IDL structure into libSVM format 
;
; 
;
;  MANDATORY ARGUMENTS :
;    morpho_table        : (input) Structure to be written
;
;    par_vec_pos         : (input) Tag names to be included
;
;    indices             : (input) Postions to be written (from clean_morpho)
;
;    filename            : (input) Output file name 
;
;   
; OPTIONAL KEYWORDS:
;    
;    CLASS_POS           : (input) morpho class tag name
;  
;    CLASS_BORDERS       : (input) morpho class range. must be numerical.
;
;    TWOCLASS            : (input) if set firts class is set to -1 and second to 1. Recommended to use this option as default. 
;
;AUTHOR:
;    $Author: huertas-company $
;-



fmt2 ='(1000(A))'


IF KEYWORD_SET(CLASS_POS) THEN BEGIN 
table_SVM=strarr((size(morpho_table))[1],(size(par_vec_pos))[1]+1) 

ENDIF ELSE BEGIN 
table_SVM=strarr((size(morpho_table))[1],(size(par_vec_pos))[1]+1)
ENDELSE

par_vec=strarr((size(par_vec_pos))[1])
j=0.
tags=tag_names(morpho_table)

for i=0., n_elements(indices)-1 do begin

;IF (where(indices EQ i))[0] NE -1 THEN BEGIN   ;check that the object should be included

IF KEYWORD_SET(CLASS_POS) THEN classif=(morpho_table.MORPHO)[indices[i]]




;extraction of the morphological parameters
for k=0,(size(par_vec_pos))(1)-1 do begin
tag_pos=where(tags EQ par_vec_pos[k])
par_vec[k]=strcompress(string(k+1)+':'+string((morpho_table.(tag_pos))[indices[i]]))
endfor

IF KEYWORD_SET(CLASS_POS) THEN BEGIN

IF KEYWORD_SET(TWOCLASS) THEN BEGIN   ; you can chose to have +1 and -1 as labels for the two classes to print the decision values
IF classif LE class_borders[1] THEN BEGIN 
table_SVM[j,*]=['-1',par_vec]
j=j+1
ENDIF ELSE BEGIN
table_SVM[j,*]=['1',par_vec]
j=j+1
ENDELSE

ENDIF

ENDIF ELSE BEGIN
table_SVM[j,*]=['1',par_vec]
j=j+1
ENDELSE



;ENDIF




NO_OBJECT:

endfor

openw,1,filename
for i=0.,j-1 do begin
printf,1,FORMAT=fmt2,transpose(table_SVM[i,*])
endfor
close,1


END


pro libSVM_read,file_name,nobj,table

;+
;NAME:
;    libSVM_READ
;
;CATEGORY :
;       Astronomy
;
;SYNTAX:
;       libSVM_read, file_name, nobj, table 
;        
;      
;
;DESCRIPTION :
; 
;Stores a libSVM file into an IDL table
;
; 
;
;  MANDATORY ARGUMENTS:
;    file_name           : (input) Name of the input file
;
;    nobj                : (input) Max number of entries
;
;    table               : (output) Output table 
;
;   
;
;
;AUTHOR:
;    $Author: huertas-company $
;-

openr,10,file_name

line=' '
un=1

flag=0
j=0

while un EQ 1 do begin
ON_IOERROR,out
readf,10,line
split=strsplit(line,':',/EXTRACT)

IF flag EQ 0 THEN BEGIN
tbl=fltarr(nobj,(size(split))(1))
flag=1
ENDIF

tbl(j,0)=float((strsplit(split(0),' ',/EXTRACT))(0))

for i=1,(size(split))(1)-2 do begin



tbl(j,i)=float((strsplit(split(i),' ',/EXTRACT))(0))

endfor

tbl(j,(size(split))(1)-1)=float(split((size(split))(1)-1))

j=j+1
endwhile

out:


table=tbl(0:j,*)

close,/a


END


FUNCTION CLEAN_MORPHO,in_tab, param, bounds,ind

;+
;NAME:
;    CLEAN_MORPHO
;
;CATEGORY :
;       Astronomy
;
;SYNTAX:
;       tab_clean=CLEAN_MORPHO(in_tab, param, bounds) 
;        
;      
;
;DESCRIPTION :
; 
;Removes from the in_tab "strange" parameter values
;
; 
;
;  MANDATORY ARGUMENTS:
;    in_tab              : (input) Name of input IDL structure
;
;    param               : (input) list of parameter names
;
;    bounds              : (input) Bounds accepted for each parameter
;(array of arrays). Limiting values are inlcuded in the accepted range. 
;
; 
;
;
;AUTHOR:
;    $Author: huertas-company $
;-


FOR i=0,(size(param))[1]-1 DO BEGIN

IF i EQ 0 THEN w=where_tag(in_tab,TAG_NAME=param[i],RANGE=[bounds[0,i],bounds[1,i]],ISELECT=ind) ELSE w=where_tag(in_tab,TAG_NAME=param[i],RANGE=[bounds[0,i],bounds[1,i]],ISELECT=w)

ENDFOR

shift_number=round(randomu(dummy_variable)*n_elements(w))
w=shift(w,shift_number)

RETURN, w


END




function eq_training_sample,morpho_train_input,param,border,init_pos,FRAC=frac

;+
;NAME:
;    eq_training_sampe
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       ind=eq_training_sample(morpho_train_input,pos,borders,FRAC=frac) 
;        
;      
;
;DESCRIPTION :
; 
;Balances training sample. Used when the training is very unbalanced
;
; 
;
;  MANDATORY ARGUMENTS :
;    morpho_train_input        : (input) Structure containing the catalog
;
;    param                     : (input) Parameter name of morphology class
;
;
;    border                    : (input) class division
;
;    
;
;
;
;
;AUTHOR:
;    $Author: huertas-company $
;-

w1=where_tag(morpho_train_input,TAG_NAME=param,RANGE=[border[0],border[1]],ISELECT=init_pos)
w2=where_tag(morpho_train_input,TAG_NAME=param,RANGE=[border[1]+.01,border[2]],ISELECT=init_pos)
w=[w1,w2]

n_ell=n_elements(w1)
n_spi=n_elements(w2)

IF KEYWORD_SET(FRAC) THEN n_obj=round(min([n_ell/(1-FRAC),n_spi/(FRAC)])) 
w1_sel=w1[0:round(n_obj*(1-FRAC))-1]
w2_sel=w2[0:round(n_obj*FRAC-1)]
w=[w1_sel,w2_sel]
return,w


END


;Copyright 2011 Marc Huertas-Company

;This file is part of GALSVM.

;    GALSVM is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 3 of the License, or
;    (at your option) any later version.

;    GALSVM is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.

 ;   You should have received a copy of the GNU General Public License
 ;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
