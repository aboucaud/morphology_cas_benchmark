pro strarr2intarr, strarr, intarray
;+
; NAME:
;       STRARR2INTARR
; PURPOSE:
;       Pack a string array into a int array
; CATEGORY:
;       Astronomy
; SYNTAX:
;       strarr2intarr, strarr, intarr
;-
 
split=strsplit(strarr,',',/EX)
intarray=intarr((size(split))(1))


FOR i=0, (size(split))(1)-1 do begin

intarray(i)=round(float(split(i)))

ENDFOR



end





FUNCTION change_header_numbers,header,first
;+
;NAME:
;    CHANGE_HEADER_NUMBERS
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       new_header=change_header_numbers(header,first)  
;        
;      
;
;DESCRIPTION :
; 
;Changes the numbers of the input header
;
; 
;
;  MANDATORY ARGUMENTS :
;    header               : (input) Input Header 
;    
;    first                : (input) Number associated to the first entry  
;
;
;AUTHOR:
;    $Author: huertas $
;-

n_el=n_elements(header)
new_header=header

FOR i=0,n_el-1 DO BEGIN

split=strsplit(header(i),' ',/EXTRACT)

body=split(2)
for j=3,(size(split))(1)-1 do begin
body=strcompress(body+' '+split(j))
endfor

new_entry=strcompress(split(0)+' '+string(round(float(split(1)-1)+first))+' '+body)

new_header(i)=new_entry

ENDFOR


RETURN,new_header

END




FUNCTION cumul_histo, histo

;+
;NAME:
;    CUMUL_HISTO
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       cumul=cumul_histo(histo)  
;        
;      
;
;DESCRIPTION :
; 
;Builds a cumulative histogram from the input histogram. The output histogram should be used in number_generate  
;
; 
;
;  MANDATORY ARGUMENTS :
;    histo               : (input) Input Histogram 
;
;
;
;AUTHOR:
;    $Author: huertas $
;-


cum_histo=histo*0.


for i=0,(size(histo))(1)-1 do begin
cum_histo(i)=total(histo(0:i))
endfor


return, cum_histo
END

PRO file2table,file,table,nlignes,final_header
;+
;NAME:
;    FILE2TABLE
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       file2table, file, table, nlignes, final_header  
;        
;      
;
;DESCRIPTION :
; 
;Reads a file and stores its content in an IDL table. WARNING!! The procedure assumes that all the entries in the file$  
;are numeric and that all the header entries begin by the character #
; 
;
;  MANDATORY ARGUMENTS :
;    file                : (input) Name of the file to be read
;
;    table               : (output) Table name
;
;    nlignes             : (input) Max entries in the table
;
;    header              : (output) Variable containing the header of the input file
;
;
;
;AUTHOR:
;    $Author: huertas $
;-


openr,1,file

header=strarr(400)
str_header=''

flag=1
ON_IOERROR,vide
readf,1,str_header
i=0
un=1

;while (strsplit(str_header,' ',/EXTRACT))(0) eq '#' do begin
while strpos(str_header,'#') NE -1 DO BEGIN
header(i)=str_header
readf,1,str_header
i=i+1
endwhile

if i ne 0 then begin
final_header=fltarr(i)
final_header=header(0:i-1)
endif else begin
final_header=''
endelse

i=0
print, str_header
str=strsplit(str_header,/EXTRACT,/REGEX)
data=double(str)
PRINT,data
table1=dblarr(nlignes,(size(data))(1))
table1(i,*)=data
i=1
while un eq 1 do begin

ON_IOERROR,out1

readf,1,data

table1(i,*)=data

i=float(i)
i=i+1

endwhile
out1:close,1


flag=0
table=fltarr(i,(size(data))(1))
table=table1(0:i-1,*)

vide:
if flag eq 1 then begin
table=fltarr(2,2)
table(0,0)=3000
close,/all
endif

END





pro table2file,table,header,file

;+
;NAME:
;    TABLE2FILE
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       table2file, table, header, file  
;        
;      
;
;DESCRIPTION :
; 
;Stores an IDL table into a file WARNING!! The procedure assumes that all the entries in the table$  
;are numeric.
; 
;
;  MANDATORY ARGUMENTS :
;    table               : (input) Input table
;
;    header              : (input) String variable containing the header
;
;    file                : (output) Filename
;
;
;
;
;AUTHOR:
;    $Author: huertas $
;-



;fmt2 ='(1(I),14(F),1(I),4(F),1(I),4(F),1(I),2(F),6(I),2(F),2(D),3(F))'



;fmt2 = '(1(I),10(F),7(D40),2(F),1(D40),2(F),1(D40),1(F),1(I),2(F),6(I),2(F),2(D35),3(F),7(D35),8(F),3(D),2(I),7(F))'

fmt2='(400(D40))'

 openw,1,file
for i=0.,(size(header))[1]-1 do begin
printf,1,header[i]
endfor

un=1
i=0
for i=0.,(size(table))[1]-1 do begin
printf,1,FORMAT=fmt2,$
table[i,*]
endfor

out:close,1

END

function number_generate,histo,range,r

;+
;NAME:
;    NUMBER_GENERATE
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       s=number_generate(histo, range, r)  
;        
;      
;
;DESCRIPTION :
; 
;Generates a random number following the probability distribution given by histo  
;
; 
;
;  MANDATORY ARGUMENTS :
;    histo               : (input) Histogram giving the probability density
;
;    range               : (input) X values of the probability distribution
;
;    r                   : (input) Minimium output value
;
;
;
;
;AUTHOR:
;    $Author: huertas-company $
;-


index=where(histo le r)


IF index(0) EQ -1 THEN BEGIN 
r=min(histo)
index=where(histo le r)

ENDIF
size_match=where(index EQ max(index))

 
delta=(r-histo(index(size_match)))/(histo(index(size_match)+1)-histo(index(size_match)))
s=range(index(size_match))+delta*(range(index(size_match)+1) - range(index(size_match)))


return,s

END


function extract_thumb,im,x,y,size
;+
;NAME:
;    EXTRACT_THUMB
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       s=extract_thumb(im,x,y,size)  
;        
;      
;
;DESCRIPTION :
; 
;Creates a stamp from a big image  
;
; 
;
;  MANDATORY ARGUMENTS :
;    im               : (input) image to cut
;
;    x              : (input) x coordinate in pixels
;
;    y                   : (input) y coordinate in pixels
;
;    size            : (input) size of stamp
;
;
;
;
;AUTHOR:
;    $Author: huertas-company $
;-

if size/2.-size/2 ne 0. then begin
res=im(x-size/2:x+size/2,y-size/2:y+size/2)
endif  else res=im(x-size/2:x+size/2-1,y-size/2:y+size/2-1)

return, res
end


;+
; NAME:
;      JJADD_TAG
;
; PURPOSE:
;      Add a tag to a pre-existing array of strutures
;
; CALLING SEQUENCE:
;      new = JJadd_Tag(struct, tag_name, value)
;
; INPUTS:
;      tag_name - The name of a new structure tag to be added
;      value    - The value to be assigned to the new tag
;
; KEYWORD PARAMETERS:
;
;      array_tag - Set this keyword flag if value is an array that
;                  should be inserted into a single tag, e.g. if 
;                  struct.tag_name = fltarr(10)
;
; OUTPUTS:
;
;      A new array of structures
;
; RESTRICTIONS:
;
;      Only works with anonymous structures. But the code is 46%
;      faster than the ICG routine ADD_TAG2. Tested using Tim
;      Robishaw's BENCHMARK.
;
; EXAMPLE:
;
;      struct = {a: 0., b: 'blah'}
;      new = jjadd_tag(struct, 'new', 0b)
;      help,new,/struct
;** Structure <2453f8>, 3 tags, length=32, data length=21, refs=1:
;   A               FLOAT           0.00000
;   B               STRING    'blah'
;   NEW             BYTE         0
;
; MODIFICATION HISTORY:
;
;  20 Apr 2005 - JohnJohn created 
;  20 Apr 2005 - Fixed so that NEW is actually assigned the value
;                VALUE rather than just 0. Thank's Louis!
;-
function jjadd_tag, struct, tagname, valuein, array_tag=array_tag
on_error, 2                     ;If broken, return to sender
nel = n_elements(struct)
nelv = n_elements(valuein)
ntags = n_tags(struct)
if n_params() ne 3 then $
  message,'Correct calling sequence: NEW = jjadd_tag(STRUCT, TAGNAME, VALUE)', /ioerr
tn = tag_names(struct[0])
if total(strmatch(tn, tagname)) then return, struct

if 1-keyword_set(array_tag) then begin
    if nelv ne 1 and nelv ne nel then $
      message,'VALUE must be a scalar or have the same length as STRUCT.',/ioerr

;set up the scalar place-holder variable
    newsingle = create_struct(struct[0], tagname, valuein[0])
endif else begin
    newsingle = create_struct(struct[0], tagname, valuein)
endelse
;set up the output array of structures with the new tag
newarray = replicate(newsingle, nel)
insert = 0b
if nelv eq 1 then newarray.(ntags) = valuein else begin
    value = valuein
    insert = 1b
endelse
for i = 0., nel-1 do begin
    ;;; STRUCT_ASSIGN will set the new tag to 0
    struct_assign, struct[i], newsingle, /nozero
    newarray[i] = newsingle
    if insert then if keyword_set(array_tag) then $
      newarray[i].(ntags) = value else newarray[i].(ntags) = value[i]
endfor
return, newarray
end

;+
;	NAME:
;		MM_DIL_N
;		2-dim:	BYTE, INT, LONG not in place
;			Function value is returned
;		3-dim:	only BYTE in place
;			Parameter is modified, -1 is returned
;
 function mm_dil_n,a,n
 if n_params() eq 1 then n=1
 sa=size(a)
; if sa(1+sa(0)) gt 3 then message,'Neither BYTE, INT nor LONG !'
 case sa(0) of
 1:begin
     ke=replicate(1b,n)
     return,dilate(a,ke,/gr)
   end
 2:begin
    if n ge 0 then begin
      ste=[[0,1,0],[1,1,1],[0,1,0]]
      qua=[[1,1,1],[1,1,1],[1,1,1]]
    endif else begin
      squa=[[1,1],[1,1]]
      if n eq -1 then kx=0 else kx=1
    endelse
    b=a
    if n eq 0 then return,b
    case 1 of
;    case sa(1+sa(0)) of
    1:begin
      if n lt 0 then b=dilate(temporary(b),squa,kx,kx,/gray,/preserve) $
      else for i=0,n-1 do $
        if i and 1 then b=dilate(temporary(b),qua,/gray,/preserve) $
        else b=dilate(temporary(b),ste,/gray,/preserve)
      return,b
      end
    else:begin
      for i=0,n-1 do $
        if i and 1 then b=dilate_32(temporary(b),qua) $
        else b=dilate_32(temporary(b),ste)
      return,b
      end
    endcase
   end
 3:begin
;    if sa(1+sa(0)) ne 1 then message,'Not BYTE for 3-dim !'
    if n gt 0 then begin
      ste=[[[0,0,0],[0,1,0],[0,0,0]], $
           [[0,1,0],[1,1,1],[0,1,0]], $
           [[0,0,0],[0,1,0],[0,0,0]]]
      qua=[[[1,1,1],[1,1,1],[1,1,1]], $
           [[1,1,1],[1,1,1],[1,1,1]], $
           [[1,1,1],[1,1,1],[1,1,1]]]
    endif else begin
      squa=[[[1,1],[1,1]],[[1,1],[1,1]]]
      if n eq -1 then kx=0 else kx=1
    endelse
    if n eq 0 then return,-1l
    if n lt 0 then begin
      a=dilate(temporary(a),squa,kx,kx,kx,/gray,/preserve)
;      if kx eq 1 then a=shift(temporary(a),0,0,1)
    endif else for i=0,n-1 do $
      if i and 1 then a=dilate(temporary(a),qua,/gray,/preserve) $
      else a=dilate(temporary(a),ste,/gray,/preserve)
    return,-1l
   end
 else: message,'Dimension not implemented !'
 endcase
 end
;-


function find_max,im

max_val=max(im,I)

x_max=I mod (size(im))(1)
y_max=I/(size(im))(2)

vector=fltarr(3)

vector(0)=max_val
vector(1)=x_max
vector(2)=y_max


return, vector
end



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;+
;
; NAME:
;    REMOVE_TAGS
;       
; PURPOSE:
;    remove the specified tags from input structure
;
; CALLING SEQUENCE:
;    remove_tags, oldstruct, tagnames, newstruct
;
; INPUTS: 
;    oldstruct: the original structure
;    tagnames: the names of tags to be removed (can be an array)
;
; OPTIONAL INPUTS:
;    NONE.
;
; KEYWORD PARAMETERS:
;    NONE.
;       
; OUTPUTS: 
;    newstruct: the new structure without tags.
;
; OPTIONAL OUTPUTS:
;    NONE
;
; CALLED ROUTINES:
;    
; 
; PROCEDURE: 
;    
;	
;
; REVISION HISTORY:
;    ????? Judith Racusin
;    25-OCT-2000 Modified to handle arbitrary tag types. Also error 
;          handling. Erin Scott Sheldon
;       
;                                      
;-                                       
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PRO remove_tags, struct, tagnames, newstruct

  IF n_params() EQ 0 THEN BEGIN 
      print,'Syntax - remove_tags, oldstruct, tagnames, newstruct'
      print
      print,'Use doc_library,"remove_tags"  for more help.'  
      return
  END

  ;; Figure out which tags get removed

  tags=tag_names(struct)
  n=n_elements(tags)
  tagnames=strupcase(tagnames)
  nt=n_elements(tagnames)
  IF nt EQ 1 THEN BEGIN
      t=where(tags NE tagnames[0],nw) 
      IF nw EQ n THEN BEGIN
          print,'-----------------------------------------------------'
          message,'Tag did not match, structure unchanged',/inf
          print,'-----------------------------------------------------'
          newstruct = struct
          return
      ENDIF 
  ENDIF ELSE BEGIN 
      match,tags,tagnames,m
      IF m[0] EQ -1 THEN BEGIN
          print,'-------------------------------------------------'
          message,'No tags matched, structure unchanged',/inf
          print,'-------------------------------------------------'
          newstruct=struct
          return
      ENDIF 
      nm=n_elements(m)
      IF nm EQ n THEN BEGIN 
          print,'-------------------------------------------------------------'
          message,'This would remove all tags! structure unchanged',/inf
          print,'-------------------------------------------------------------'
          newstruct=struct
          return
      ENDIF 
      t=lindgen(n)
      remove, m, t
  ENDELSE 
      
  ;; create new structure
  tags=tags[t]
  n=n_elements(tags)

  newstruct=create_struct(tags[0],struct[0].(t[0]))
  
  FOR i=1L, n-1 DO newstruct = create_struct(temporary(newstruct), $
                                             tags[i], struct[0].(t[i]) )

  newstruct=replicate( temporary(newstruct), n_elements(struct) )
  struct_assign,struct,newstruct

  return
END



;Copyright 2007 Marc Huertas-Company

;This file is part of GALSVM.

;    GALSVM is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 3 of the License, or
;    (at your option) any later version.

;    GALSVM is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.

 ;   You should have received a copy of the GNU General Public License
 ;   along with this program.  If not, see <http://www.gnu.org/licenses/>.


