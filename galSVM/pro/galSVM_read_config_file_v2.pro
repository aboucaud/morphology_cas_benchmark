function  galSVM_read_config_file,FILENAME=filename

IF NOT KEYWORD_SET(filename) THEN filename='config_file.txt'

ls=file_search(filename)
IF ls[0] EQ '' THEN BEGIN 
print, "I cannot find the configuration file in your current folder. Stopping..."
config = CREATE_STRUCT('dummy', 0)
GOTO, OUT
ENDIF
 
openr,1,filename
;openw,2,"input_parameters_batch"
un=1
line=' '


FLAG=0

while un EQ 1 do begin
ON_IOERROR,out

readf,1,line


IF (strpos(line,'#'))(0) EQ -1 AND (strpos(line,':'))(0) NE -1 THEN BEGIN

split_string=strsplit(line,':',/EX)
var_name=split_string(0)
var_value=split_string(1)

IF FLAG EQ 0 THEN BEGIN
config = CREATE_STRUCT(var_name, var_value)  
FLAG=1
ENDIF

IF FLAG EQ 1 THEN config=JJADD_TAG(config,var_name,var_value)

ENDIF


ENDWHILE
out:
close,/a

RETURN,config




END
