FUNCTION galSVM_remove_stars, seed, im_in, mask,  gal_number, size_th $
                      ,SEX=sex_file, THRESHOLD=threshold
;+
;NAME:
;    galSVM_REMOVE_STARS
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       gal_no_stars = remove_stars(im_in, mask, gal_number, size_th, [SEX,THRESHOLD])
;
;DESCRIPTION :
; 
;it just removes foreground stars. It is a preliminary step before simulating a 
; high redshift galaxy from a local one.
;
;  MANDATORY ARGUMENTS :
;    im_in        : (input) image of the local galaxy with stars to be removed
;
;    mask         : (input) SExtractor segmentation map
;
;    gal_number   : (input) SExtractor NUMBER of the galaxy
;
;    OPTIONAL KEYWORDS:
;
;      THRESHOLD  : (input) Minimal size of the object to be kept      
;
;AUTHOR :
;    $Author: huertas $
;-

IF KEYWORD_SET(sex_file) THEN sex_cat=mrdfits(sex_file,1)


bg_level=median(im_in[where(mask EQ 0.)])
print, 'BG_LEVEL:', bg_level
;stop
bg_sig=sqrt(variance(im_in[where(mask EQ 0.)]))
rand_noise=randomn(seed,(size(im_in))[1],(size(im_in))[1])*bg_sig+bg_level

im_no_stars=im_in


IF  KEYWORD_SET(sex_file) THEN BEGIN
ximage=sex_cat.X_IMAGE
yimage=sex_cat.Y_IMAGE
number=sex_cat.NUMBER
xgal=ximage[where(number EQ 0)]
ygal=yimage[where(number EQ 0)]


x_ref=xgal-(size(im_in))[1]/2
y_ref=ygal-(size(im_in))[2]/2

ENDIF ELSE BEGIN

;otherwise the galaxy is supposed to be at the image center
xgal=(size(im_in))(1)/2
ygal=(size(im_in))(2)/2
IF (where(mask NE 0. and mask NE gal_number))[0] EQ -1 THEN GOTO,SORTIE
indices=mask(where(mask NE 0. and mask NE gal_number))
indices_uniq=indices(uniq(indices))
IF min(indices_uniq) EQ max(indices_uniq) THEN BEGIN  ;if there is only one value
value=indices_uniq
indices_uniq=fltarr(1)
indices_uniq[0]=value
ENDIF

ENDELSE


IF KEYWORD_SET(sex_file) THEN BEGIN
for i=0,(size(sex_cat))[1]-1 do begin

s_g=sex_cat[i,3]
area=sex_cat[i,12]
IF (sex_cat[i,0] EQ gal_number OR s_g LT 0.5) AND area GT threshold THEN GOTO,fin
;AND area GT threshold
x_obj=sex_cat[i,1]-x_ref
y_obj=sex_cat[i,2]-y_ref


IF abs(x_obj-(xgal-x_ref)) GT (size(im_in))[1]/2 OR abs(y_obj-(ygal-y_ref)) GT (size(im_in))[2]/2 THEN GOTO,FIN
IF x_obj+size_th/2 GE (size(im_in))[1] OR x_obj-size_th/2 LT 0 OR y_obj+size_th/2 GE (size(im_in))[2] OR y_obj-size_th/2 LT 0 THEN BEGIN 
im_no_stars[where(mask EQ sex_cat[i,0])]=rand_noise[where(mask EQ sex_cat[i,0])]
GOTO,fin
ENDIF
star_th=extract_thumb(mask,x_obj,y_obj,size_th)



IF (where(star_th EQ gal_number))[0] NE -1 THEN BEGIN  ;if the star is within the galaxy area
star=extract_thumb(im_in,x_obj,y_obj,size_th)
gal_median=median(star[where(star_th EQ gal_number)])
im_no_stars[where(mask EQ sex_cat[i,0])]=gal_median 
ENDIF ELSE BEGIN  ;otherwise..
im_no_stars[where(mask EQ sex_cat[i,0])]=rand_noise[where(mask EQ sex_cat[i,0])]
ENDELSE
fin:
endfor
ENDIF ELSE BEGIN
for i=0,(size(indices_uniq))[1]-1 do begin

obj_cord=array_indices(mask,(where(mask EQ indices_uniq[i]))[0])
x_obj=obj_cord[0]
y_obj=obj_cord[1]

IF x_obj+size_th/2 GE (size(im_in))[1] OR x_obj-size_th/2 LT 0 OR y_obj+size_th/2 GE (size(im_in))[2] OR y_obj-size_th/2 LT 0 THEN BEGIN 
im_no_stars[where(mask EQ indices_uniq[i])]=rand_noise[where(mask EQ indices_uniq[i])]
GOTO,fin2
ENDIF
star_th=extract_thumb(mask,x_obj,y_obj,size_th)

IF (where(star_th EQ gal_number))[0] NE -1 THEN BEGIN  ;if the star is within the galaxy area
star=extract_thumb(im_in,x_obj,y_obj,size_th)
gal_median=median(star[where(star_th EQ gal_number)])
im_no_stars[where(mask EQ indices_uniq[i])]=gal_median 
ENDIF ELSE BEGIN  ;otherwise..
im_no_stars[where(mask EQ indices_uniq[i])]=rand_noise[where(mask EQ indices_uniq[i])]
ENDELSE
fin2:
endfor

ENDELSE
SORTIE:
;stop
return,im_no_stars


END





FUNCTION galSVM_degrade_resolution, im, hz_res, lz_res, zl, zh, lz_pix_scale,PSF_LOW=psf_low

;+
;NAME:
;    galSVM_DEGRADE_RESOLUTION
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       gal_deg = degrade_resolution(im, hz_res, lz_res, zl, zh, lz_pix_scale, [PSF_LOW])
;
;DESCRIPTION :
; 
;degrades the resolution of the local galaxy image to reach the high redshift one. 
; 
;
;  MANDATORY ARGUMENTS :
;    im          : (input) image of the local galaxy
;
;    hz_res       : (input) mean resolution of the high z image
;
;    lz_res       : (input) mean resolution of the low z image
;
;    zl           : (input) low z value
;
;    zh           : (input) high z value 
;
;    lz_pix_scale : (input) pixel scale of the input image
;
;    OPTIONAL KEYWORDS:
;
;      PSF_LOW  : (input) image of a local PSF. If set, lz_res is ignored     
;
;AUTHOR :
;    $Author: huertas-company $
;-

;scale in kpc/" for the computed redshift
scale=1/zang(1,zh)

;resolution in kpc
res_kpc=hz_res*scale


;correspondance at low redshift
scale_low=zang(1,zl)
fwhm_low=res_kpc*scale_low

fwhm_low_pix=fwhm_low/lz_pix_scale

IF KEYWORD_SET(psf_low) THEN BEGIN
gauss_fit=gauss2dfit(psf_low,fit_result)
fwhm_start_pix=fit_result[2]*2.35
print, 'LOW Z RESOLUTION FROM PSF:',fwhm_start_pix
ENDIF ELSE BEGIN
fwhm_start_pix=lz_res/lz_pix_scale
ENDELSE

;I build the low z PSF
IF fwhm_low_pix^2.-fwhm_start_pix^2. LE 0. THEN BEGIN 
PRINT,'I DID NO CONVOLUTION AT ALL!'
im_con=im
ENDIF ELSE BEGIN
PSF=psf_gaussian(NPIXEL=(size(im))[1],FWHM=sqrt(fwhm_low_pix^2.-fwhm_start_pix^2.),/NORMALIZE)

im_con=convolve(im,PSF)
ENDELSE

print, "FLUXES:", total(im),total(im_con)

RETURN,im_con
END





FUNCTION galSVM_build_stamps, im, hz_pix_scale, lz_pix_scale, hz_zerop, zh, zl,mag, sigma_ref, lz_gal_size_pix,AREA=area,MAG_LZ=mag_lz,QEVOL=qevol,morpho_type
                         
;+
;NAME:
;    galSVM_BUILD_STAMPS 
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       stamp = build_thumbnails(im, pix_scale, lz_pix_scale, hz_zerop, 
;       mag, sigma_ref, lz_gal_size_pix, morpho_type)
;
;DESCRIPTION :
; 
;simulates a high-z stamp from a low-z one
; 
;
;  MANDATORY ARGUMENTS :
;    im_in        : (input) image of the local galaxy with stars to be removed
;
;    mask         : (input) SExtractor segmentation map
;
;    gal_number   : (input) SExtractor NUMBER of the galaxy
;
;    OPTIONAL KEYWORDS:
;
;      THRESHOLD  : (input) Minimal size of the object to be kept      
;
;AUTHOR :
;    $Author: huertas-company $
;-


CATCH,error_status

IF error_status NE 0 THEN BEGIN
PRINT, 'Something went wrong while redshifiting the galaxy.'
return,fltarr(5,5)+(-1)
ENDIF



lz_gal_size_pix=1
lz_gal_size_kpc=1./zang(1,zl)*lz_gal_size_pix*lz_pix_scale
alpha=1.
hz_gal_size_pix=zang(alpha(0)*lz_gal_size_kpc,zh)/hz_pix_scale


bin_factor=lz_gal_size_pix/hz_gal_size_pix
bin_factor=1/zang(1,zh)*hz_pix_scale/(1/zang(1,zl)*lz_pix_scale)
print, 'BIN FACTOR: ', bin_factor




IF bin_factor LT 1. THEN BEGIN
print,'WARNING: binning factor < 1 ... INTERPOLATING'
bin_factor_old=bin_factor
;bin_factor=1
ENDIF

bin_factor=bin_factor[0]
HELP, im



im_binned=congrid(im,(size(im))[1]/(bin_factor),(size(im))[2]/(bin_factor),CUBIC=-0.5,/CENTER)*(bin_factor)*(bin_factor)
HELP, im_binned


;;;Then scaling to the new magnitude


arcsec_kpc=zang(1,zl)^2
arcsec_kpc2=zang(1,zh)^2

IF KEYWORD_SET(area) THEN BEGIN 
print,'USING COSMOLOGICAL DIMMING TO ESTIMATE MAG AT HIGH Z. EVOLUTION COEFFICIENT IS SET TO '+string(qevol,FORMAT='(F3.1)')
;stop
flux=total(im)
lz_zerop=2.5*alog10(flux)+(mag_lz)  ;correcting for evolution
print,'LZ_ZEROP: ', lz_zerop
sb=flux/area
sbhz=sb/(1+zh)^4
new_area=area/arcsec_kpc*arcsec_kpc2
new_flux=sbhz*(new_area)
mag=-2.5*alog10(new_flux)-qevol*zh+lz_zerop
new_flux=10^((hz_zerop-mag)/2.5)
PRINT,'ESTIMATED MAGNITUDE AFTER DIMMING: ', mag
ENDIF ELSE BEGIN

flux=total(im_binned)
IF flux LT 0 THEN stop
new_flux=10^((hz_zerop-mag)/2.5)

ENDELSE


PRINT,'MAGNITUDE: ',mag
PRINT, 'FLUX:', flux
PRINT,'NEW FLUX:', new_flux


IF n_elements(im_binned) LT 5 OR flux LT 0 THEN BEGIN
im_binned(0,0)=-1
im_binned_scale=im_binned
GOTO,OUT
ENDIF

;I extract a piece of background
bg=im_binned[0:(size(im_binned))[1]/4,0:(size(im_binned))[2]/4]


IF n_elements(bg) LT 5 THEN BEGIN
im_binned(0,0)=-1
im_binned_scale=im_binned
GOTO,OUT
ENDIF

;the background noise of the image must be at least 3 times lower than
;the reference image noise. If it is not the case, I increase the
;magnitude of the simulated galxy. This can cause problems if you want
;to simulate very bright objects.

print, 'SIGMA:',sigma_ref
PRINT, 'SIGMA_BG:', new_flux/flux*robust_sigma(bg)
IF new_flux/flux*robust_sigma(bg) GT sigma_ref/2. THEN BEGIN 
mag_before=hz_zerop-2.5*alog10(new_flux)
new_flux=(sigma_ref/2.)*flux/sqrt(variance(bg))
mag=hz_zerop-2.5*alog10(new_flux)
print, 'CHANGING MAGNITUDE...'
print, 'MAG BEFORE:',mag_before
print, 'MAG_AFTER:',mag
ENDIF

im_binned_scale=im_binned/flux*new_flux
HELP,NEW_FLUX

; I put the median to 0
im_binned_scale=im_binned_scale-median(bg)*new_flux/flux

;IF im_binned_scale[(size(im_binned_scale))[1]/2.0,(size(im_binned_scale))[2]/2.0] LT 0 THEN stop

OUT:
RETURN,im_binned_scale

END




pro galSVM_adapt_catalog,seed,local_path,highz_path,cat_table,size_x,hz_pix_scale,lz_pix_scale,hz_zerop,hz_fwhm,lz_fwhm,sigma_ref,output_name,Z_HISTO=z_histo,MAG_HISTO=mag_histo,LAMBDA_OBS=lambda_obs,MULTI_BAND=multi_band,INV=multi_band_inv,PSF=PSF,DIMMING=DIMMING

;+
;NAME:
;    galSVM_ADAPT_CATALOG
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       adapt_catalog,seed, cat_filename, hz_pix_scale, lz_pix_scale, 
;       hz_zerop, hz_fwhm, lz_fwhm, sigma_ref, output_name, Z_MEAN=z_mean, 
;       MEAN_MAG=mean_mag, Z_HISTO=z_histo, MAG_HISTO=mag_histo
;
;DESCRIPTION :
; 
;Gets a local catalog and puts it at high redshift by reproducing the physical and instrumental 
;properties of the high z sample to be analyzed. It creates a new
;catalog with the properties of the sample created.
; 
;
;  MANDATORY ARGUMENTS :
;    seed        : (input) integer number for random realizations
;
;    local_path  : (input) Path for the local sample.
;
;    cat_table   : (input) Structure containing the catalog 
;
;    size_x      : (input) Number of objects to be simulated. They are
;taken randomly from the big catalog.
;
;    hz_pix_scale: (input) Pixel size of the high z sample
;
;    lz_pix_scale: (input) Pixel size of the local sample
;
;    hz_zerop    : (input) Photometric zeropoint of the high-z sample
;
;    hz_fwhm     : (input) Mean resolution of the high-z sample
;
;    lz_fwhm     : (input) Mean resolution of the low-z sample
;
;    sigma_ref   : (input) Standard deviation of the high-z background 
;
;    output_name  : (input) Filename of the output catalog
;
;    OPTIONAL KEYWORDS:
;
;
;     Z_HISTO   : (input) Redshift histogram of the high-z sample. Not
;really an option in this last version. If not provided the
;function won't work.
;      
;    MAG_HISTO  : (input) Magnitude histogram of the high-z sample.  Not
;really an option in this last version. If not provided the
;function won't work.
; 
;
;    MULTI_BAND_INV : (input) To drop in different bands
;(e.g. ALHAMBRA). Not yet fully implemented.
;
;AUTHOR :
;    $Author: huertas-company $
;-


cd, local_path ;change current dir to local path

size_y=n_tags(cat_table)
posit=indgen(n_elements(cat_table.INDEX))
shift_number=randomu(seed)*n_elements(cat_table.INDEX)
posit=shift(posit,shift_number)
cat_table=cat_table[posit[0:size_x-1]] ;cut catalog 

magnitude=fltarr(size_x)
zdist=fltarr(size_x)

number=cat_table.INDEX
zl=cat_table.REDSHIFT  ;the low z vector
gal_sizes=cat_table.SIZE ;the sizes
morpho_types=cat_table.MORPHO ;the morphology
image_names=cat_table.IMAGE_NAME
mask_names=cat_table.MASK_NAME

IF KEYWORD_SET(DIMMING) THEN BEGIN 
area=cat_table.Area  ;area of the galaxy in arcsec square
mag_lz=fltarr(n_elements(area))
;mag_lz[*,0]=cat.modelmag_u
;mag_lz[*,1]=cat.modelmag_g
;mag_lz[*,2]=cat.modelmag_r
;mag_lz[*,3]=cat.modelmag_i
;mag_lz[*,4]=cat.modelmag_z
ENDIF

IF KEYWORD_SET(PSF) THEN psfs_names=cat_table.PSF_NAME

;THE REDSHIFTS

;reproduce a given redshift distribution
IF KEYWORD_SET(Z_HISTO) THEN BEGIN
z_histo_norm=z_histo(*,1)/total(z_histo(*,1)) ;normalize
z_histo_cumul=cumul_histo(z_histo_norm)

uni_rand=randomu(seed,size_x)

for i=0,size_x-1 do begin
zdist(i)=number_generate(z_histo_cumul,z_histo(*,0)+z_histo(1,0)-z_histo(0,0),uni_rand(i))
IF zdist(i) LT zl(i) THEN zdist(i)=zl(i) ;to be sure...
endfor

ENDIF ELSE BEGIN
PRINT, "ERROR: you must provide a redshist distribution"
GOTO, FINAL_OUT
ENDELSE



;THE MAGNITUDES

;reproduce a magnitude distribution. the magnitude is assigned
;according to the median z-mag relation. so given a redshift I look
;for the median apparent magnitude expected in the real dataset and
;associate that mag + scatter

IF (KEYWORD_SET(MAG_HISTO) AND KEYWORD_SET(MULTI_BAND)) THEN BEGIN
;print, 'histogram generator'
;mag_histo_norm=mag_histo(*,1)/total(mag_histo(*,1)) ;normalize
;mag_histo_cumul=cumul_histo(mag_histo_norm)

size_x_tmp=size_x
uni_rand=randomu(size_x_tmp,size_x)

for i=0,size_x-1 do begin

magnitude[i]=interpol(mag_histo[*,1],mag_histo[*,0],zdist[i])+interpol(mag_histo[*,2],mag_histo[*,0],zdist[i])*RANDOMN(seed,1)

ENDFOR
ENDIF ELSE BEGIN
PRINT, "ERROR: you must provide a magnitude distribution"
GOTO, FINAL_OUT
ENDELSE


; the local catalog is at a fixed wavelength, I am moving the high z one
;For each field I have a different magnitude distribution


;update catalog
IF tag_exist(cat_table,'HIGHZ') EQ 1 THEN BEGIN REMOVE_TAGS,cat_table,['HIGHZ','HIGHZ_MAG'],new_cat_table
cat_table=new_cat_table
ENDIF
cat_table=JJADD_TAG(cat_table,'HIGHZ',zdist)
IF NOT KEYWORD_SET(DIMMING) THEN cat_table=JJADD_TAG(cat_table,'HIGHZ_MAG',magnitude)



IF KEYWORD_SET(INV_MULTI_BAND) THEN BEGIN
print,'I am in the inverse multi band mode'
n_bands=n_elements(multi_band_inv.lambdas)
z_cent=multi_band_inv.lambdas/lambda_obs-1  ;central redshifts correspondig to each band

mag_table=fltarr(size_x,n_bands)

for k=0,n_bands-1 do begin

mag_norm=mag_histo(*,k+1)/total(mag_histo(*,k+1))
mag_cumul=cumul_histo(mag_norm)

FOR i=0,size_x-1 DO BEGIN
mag_table[i,k]=number_generate(mag_cumul,mag_histo(*,0)+(mag_histo(1,0)-mag_histo(0,0)),uni_rand(i))
ENDFOR

endfor

filter=fltarr(size_x)
FOR i=0,size_x-1 DO BEGIN
min_dist=min(abs(z_cent-zdist(i)),pos_min)

magnitude(i)=mag_table(i,pos_min)
filter(i)=pos_min

ENDFOR

IF tag_exist(cat_table,'HIGHZ') EQ 1 THEN BEGIN 
REMOVE_TAGS,cat_table,['HIGHZ','HIGHZ_MAG','FILTER'],new_cat_table
cat_table=new_cat_table
ENDIF
cat_table=JJADD_TAG(cat_table,'HIGHZ',zdist)
cat_table=JJADD_TAG(cat_table,'HIGHZ_MAG',magnitude)
cat_table=JJADD_TAG(cat_table,'FILTER',filter)


ENDIF


size_gal_binned=fltarr(size_x) ;will contain the sizes of the binned images
name_redshifted=strarr(size_x) ;will contain the names of the redshifted stamps



filter=strarr(size_x)
for i=0,size_x-1 do begin

close=0.
my_num=number[i]
imname=image_names[i]
IF KEYWORD_SET(PSF) THEN psfname=psfs_names[i]
maskname=mask_names[i]
gal_size=gal_sizes[i]

IF KEYWORD_SET(INV_MULTI_BAND) THEN BEGIN
pos_min=cat_table.filter[i]
hz_fwhm=multi_band_inv.fwhms[pos_min]
hz_pix_scale=multi_band_inv.pix_scale[pos_min]
hz_zerop=multi_band_inv.zerop[pos_min]
sigma_ref=multi_band_inv.sigma[pos_min]

image_dir=strcompress("./stamps/")
psf_dir=strcompress("./psfs/")
mask_dir=strcompress("./masks/")

PRINT, "THE REDSHIFT IS Z=",zdist(i), "I AM SIMULATING THE ",multi_band_inv.names(pos_min)," band"
PRINT, "THE ZEROPOINT IS: ", hz_zerop
PRINT, "THE FWHM is: ", hz_fwhm
PRINT, 'THE SIGMA BG is: ', sigma_ref
ENDIF


IF KEYWORD_SET(MULTI_BAND) THEN BEGIN

; the high z catalog is at a fixed wavelength, I am moving the low z one
;Single magnitude distribution

n_bands=n_elements(multi_band.lambdas)
z_cent=lambda_obs/multi_band.lambdas-1
min_dist=min(abs(z_cent-zdist(i)),pos_min) ;compute the local filter to be used

;get directory names
filter[i]=multi_band.names(pos_min)
image_dir=strcompress(multi_band.names(pos_min)+"/stamps/")
psf_dir=strcompress(multi_band.names(pos_min)+"/psfs/")
mask_dir=strcompress(multi_band.names(pos_min)+"/masks/")

lz_fwhm=multi_band.fwhms[pos_min]
print, multi_band.lambdas
lz_pix_scale=multi_band.pix_scale[pos_min]
;gal_size=gal_sizes[i,pos_min]
;flux=fluxes[i,pos_min]

PRINT, "THE REDSHIFT IS Z=",zdist(i), "I AM USING THE ",multi_band.names(pos_min)," image" 

IF KEYWORD_SET(DIMMING) THEN BEGIN

print, 'DIMMING mode, choosing the rest-frame apparent magnitude'

IF multi_band.names(pos_min) EQ 'u' THEN mag_lz[i]=cat_table[i].modelmag_u
IF multi_band.names(pos_min) EQ 'g' THEN mag_lz[i]=cat_table[i].modelmag_g
IF multi_band.names(pos_min) EQ 'r' THEN mag_lz[i]=cat_table[i].modelmag_r
IF multi_band.names(pos_min) EQ 'i' THEN mag_lz[i]=cat_table[i].modelmag_i
IF multi_band.names(pos_min) EQ 'z' THEN mag_lz[i]=cat_table[i].modelmag_z



ENDIF

ENDIF


;reads data
cd, image_dir
im=readfits(strcompress(imname,/REM)) ;reads the image
cd, '../..'
IF KEYWORD_SET(PSF) THEN BEGIN
cd, psf_dir
psf=readfits(strcompress(psfname,/REM))
psf=psf/total(psf) ; PSF normalization
cd, '../..'
ENDIF
cd, mask_dir
chk=readfits(strcompress(maskname,/REM))


;I remove the background median
IF (where(chk EQ 0))(0) EQ -1 THEN BEGIN 
name_redshifted[i]='none'
print, 'Something went wrong while creating the stamp. Object: '+ string(i)
GOTO,NO_OBJECT
ENDIF

im=im-median(im[where(chk EQ 0.)])

print, 'WITHOUT MEDIAN:', total(im)

;I look for the galaxy position 
gal_number=chk[(size(im))[1]/2,(size(im))[2]/2]
gal_area=n_elements(where(chk EQ gal_number))
flux=total(im[where(chk EQ gal_number)])
gal_no_stars=galSVM_remove_stars(seed,im,chk,gal_number,32)

;I degrade the resolution

;first I drop the galaxy in a bigger background:


zh=zdist(i)

print, hz_fwhm


DEGRADE:
IF KEYWORD_SET(PSF) THEN BEGIN 
IF zl[i] LT 0. THEN gal_deg=galSVM_degrade_resolution(gal_no_stars,hz_fwhm,lz_fwhm,0.05,zh,lz_pix_scale,PSF_LOW=psf) ELSE gal_deg=galSVM_degrade_resolution(gal_no_stars,hz_fwhm,lz_fwhm,zl[i],zdist[i],lz_pix_scale,PSF_LOW=psf) 
ENDIF ELSE BEGIN
IF zl[i] LT 0. THEN gal_deg=galSVM_degrade_resolution(gal_no_stars,hz_fwhm,lz_fwhm,0.05,zh,lz_pix_scale) ELSE gal_deg=galSVM_degrade_resolution(gal_no_stars,hz_fwhm,lz_fwhm,zl[i],zdist[i],lz_pix_scale) 
ENDELSE

;stop

;I bin the galaxy
IF zl[i] LT 0. THEN BEGIN 
zlow=0.05
ENDIF ELSE BEGIN
IF not KEYWORD_SET(DIMMING) THEN mag=(cat_table.HIGHZ_MAG)[i]
zlow=(cat_table.REDSHIFT)[i]
ENDELSE


IF morpho_types[i] LT 2 THEN morpho=1 ELSE morpho=2

IF KEYWORD_SET(DIMMING) THEN BEGIN
gal_binned=galSVM_build_stamps(gal_deg,hz_pix_scale,lz_pix_scale,hz_zerop,zh,zlow,mag,sigma_ref,gal_size,morpho,AREA=area[i],MAG_LZ=mag_lz[i],QEVOL=dimming)
magnitude[i]=mag
ENDIF ELSE BEGIN
gal_binned=galSVM_build_stamps(gal_deg,hz_pix_scale,lz_pix_scale,hz_zerop,zh,zlow,mag,sigma_ref,gal_size,morpho)
help, 'GAL_DEG', gal_deg, gal_binned
Endelse

IF gal_binned(0,0) EQ -1 THEN BEGIN 
PRINT,'I COULD NOT CREATE THE OBJECT, something went wrong. Ignoring...'
name_redshifted[i]='none'
GOTO,NO_OBJECT
ENDIF


;I write the galaxy
number_to_write=string(floor(my_num),FOR='(I05)')
writefits,strcompress(highz_path+'redshifted_'+number_to_write+'.fits',/rem),gal_binned

size_gal_binned[i]=(size(gal_binned))[1]
name_redshifted[i]=strcompress(highz_path+'redshifted_'+number_to_write+'.fits',/rem)



NO_OBJECT:

IF NOT KEYWORD_SET(INV_MULTI_BAND) THEN BEGIN 
print, "CHANGING TO LOCAL PATH..."
cd, local_path 
ENDIF

ENDFOR


IF KEYWORD_SET(DIMMING) THEN cat_table=JJADD_TAG(cat_table,'HIGHZ_MAG',magnitude)

IF tag_exist(cat_table,'BINNEDSIZE') EQ 1 THEN BEGIN 
REMOVE_TAGS,cat_table,['BINNEDSIZE','IMAGEHZNAME','FILTER'],new_cat_table
cat_table=new_cat_table
ENDIF
cat_table=JJADD_TAG(cat_table,'BINNEDSIZE',size_gal_binned)
cat_table=JJADD_TAG(cat_table,'IMAGEHZNAME',name_redshifted)

IF NOT KEYWORD_SET(INV_MULTI_BAND) THEN cat_table=JJADD_TAG(cat_table,'FILTER',filter)



mwrfits,cat_table,strcompress(highz_path+output_name,/rem),/CREATE


FINAL_OUT:


END




function galSVM_find_possible_location,mask,size_box,size_im

;+
;NAME:
;    galSVM_FIND_POSSIBLE_LOCATION 
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       loc=galSVM_find_possible_location(mask, size_box, size_im)
;
;DESCRIPTION :
; 
;Finds a free zone in the field image in order to drop the simulated galaxy
; 
;
;  MANDATORY ARGUMENTS :
;    mask         : (input) Segmentation map
;
;    size_box     : (input) 
;
;    size_im      : (input) Image size
;
;
;AUTHOR :
;    $Author: huertas-company $
;-


COMMON random_state,seed_var

size_x=(size(mask))[1]
size_y=(size(mask))[2]



;bigger support
mask_big=fltarr(size_x+2*size_im[0],size_x+2*size_im[1])+1.
mask_big[size_im[0]:size_im[0]+size_x-1,size_im[1]:size_im[1]+size_y-1]=mask

;convolution function
con_function=mask_big*0.
con_function[0:size_box[0]-1,0:size_box[1]-1]=1.

mask_con=float(fft(fft(mask_big,-1)*conj(fft(con_function,-1)),1)*(size_x+size_im[0])*(size_x+size_im[0]))

loc=mask*0.
mask_con[where(mask_con GE 1.)]=0.
IF (where(mask_con NE 0.))[0] EQ -1 THEN return,[-1,-1]
mask_con[where(mask_con NE 0.)]=1.



pos_lin=where(mask_con EQ 1.)

random_pos=randomu(seed_var)*(size(pos_lin))[1]

pos=array_indices(mask_con,pos_lin[random_pos])

pos=[pos[0]-size_im[0],pos[1]-size_im[1]]


return,pos
END




pro galSVM_drop_galaxies,seed,highz_path,cat_filename,field,hfield,mask,max_objects,zerop,gain,pixel_scale,seeing,satur_level,image_root,dropped_catalog, sex_command, sex_file, COMPRESS=compress

;+
;NAME:
;    galSVM_DROP_GALAXIES 
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       galSVM_drop_galaxies,seed, cat_filename, field, mask, max_objects, image_root, dropped_catalog, sex_command, sex_file
;
;DESCRIPTION :
; 
;Drops the stamps created by galSVM_adapt_catalog in the high-z image
; 
;
;  MANDATORY ARGUMENTS :
;    seed        : (input) integer number for random realizations
;
;    cat_filename: (input) Name of the catalog file created by galSVM_adapt_catalog
;
;    field:        (input) Image of the high-z field
;
;    mask:         (input) Segmentation image from SExtractor of the high-z field
;
;    sex_command:  (input) SExtractor command (generally: "sex")
;
;    sex_file:     (input) SExtractor .sex file
;
;AUTHOR :
;    $Author: huertas-company $
;-


cd, highz_path

;I read the catalog

cat_table=mrdfits(cat_filename,1)

openw,1,'sex_khz.sh'


;initialization
n=(size(cat_table.INDEX))[1]
size_y=n_tags(cat_table)
field_numberv=cat_table.INDEX*0.
xposv=field_numberv
yposv=field_numberv
rasimuv=field_numberv
decsimuv=field_numberv


;size_max=max(cat_table[*,nparam-1])



i=0
k=0
gal_per_field=0

drop_field=field
drop_mask=mask

field_size_x=(size(field))[1]
field_size_y=(size(field))[2]
thumb_size=200


WHILE i LT n do begin


bucle_start:
gal_per_field=gal_per_field+1
print,'GALAXY NUMBER',i
print,'GALAXIES IN FIELD ',k,': ',gal_per_field
IF gal_per_field GT max_objects THEN GOTO,WRITING 
IF i GE n THEN GOTO,WRITING
my_num=(cat_table.INDEX)[i]
print, "MY NUM ",my_num


image_namev=cat_table.IMAGEHZNAME
image_name=strcompress(image_namev[i],/rem)
print, strcompress(image_name,/rem)


;I check that the image name was written in the catalog
IF image_name EQ 'none' OR strmatch(image_name,'*redshifted*') EQ 0 THEN GOTO,OUT1

;I check that the image name exists before reading. Otherwise I ingore
;this image and continue with the next one.
ls=file_search(image_name)
IF ls[0] EQ '' THEN GOTO,OUT1

;If everything is all right, I get the stamp 
im=readfits(image_name)
IF (size(im))(1) GE (size(field))(1) THEN GOTO,OUT1
imsize_x=(size(im))[1]
imsize_y=(size(im))[2]

seed_x=seed
seed_y=seed*5.
seed_loc=seed/5.

;look for a position
look_times=0
looking:
look_times=look_times+1

IF look_times GT 10 THEN GOTO,writing

posx=randomu(seed_x)*(field_size_x-(thumb_size+imsize_x))+thumb_size/2
seed=posx/10.
posy=randomu(seed_y)*(field_size_y-(thumb_size+imsize_y))+thumb_size/2
seed=posy/100.
mask_th=extract_thumb(drop_mask,posx,posy,thumb_size)

loc=galSVM_find_possible_location(mask_th,[30,30],[imsize_x,imsize_y])

IF loc[0] EQ -1 THEN GOTO,looking



;refreshing the big field

drop_field_cut=drop_field[posx-thumb_size/2+loc[0]:posx-thumb_size/2+loc[0]+imsize_x-1,posy-thumb_size/2+loc[1]:posy-thumb_size/2+loc[1]+imsize_y-1]
drop_mask_cut=drop_mask[posx-thumb_size/2+loc[0]:posx-thumb_size/2+loc[0]+imsize_x-1,posy-thumb_size/2+loc[1]:posy-thumb_size/2+loc[1]+imsize_y-1]

resistant_mean,drop_field_cut,1.0,median_zone,sigma_zone,num

PRINT, 'MEDIAN:', median_zone


drop_field[posx-thumb_size/2+loc[0]:posx-thumb_size/2+loc[0]+imsize_x-1,posy-thumb_size/2+loc[1]:posy-thumb_size/2+loc[1]+imsize_y-1]=drop_field[posx-thumb_size/2+loc[0]:posx-thumb_size/2+loc[0]+imsize_x-1,posy-thumb_size/2+loc[1]:posy-thumb_size/2+loc[1]+imsize_y-1]+im


drop_mask[posx-thumb_size/2+loc[0]:posx-thumb_size/2+loc[0]+imsize_x-1,posy-thumb_size/2+loc[1]:posy-thumb_size/2+loc[1]+imsize_y-1]=1


xfond=posx-thumb_size/2+loc[0]+imsize_x/2
yfond=posy-thumb_size/2+loc[1]+imsize_y/2


field_numberv[i]=k+1
xposv[i]=xfond
yposv[i]=yfond
xyad,hfield,xfond,yfond,rasimu,decsimu
rasimuv[i]=rasimu
decsimuv[i]=decsimu



OUT1:
i=i+1


GOTO,bucle_start

WRITING:
gal_per_field=0
print, 'WRITING: ',i
num2write=string(k+1,FOR='(I05)')

weird_values=where(finite(drop_field) EQ 0)

IF weird_values[0] NE -1 THEN drop_field[where(finite(drop_field) EQ 0)]=field[where(finite(drop_field) EQ 0)]; just in case

writefits,strcompress(image_root+num2write+'.fits',/rem),drop_field,hfield


printf,1,strcompress(sex_command+' '+strcompress(highz_path+image_root+num2write+'.fits',/rem)+ ' -c '+sex_file+ ' -CATALOG_NAME '+strcompress(highz_path+image_root+num2write+'.fit',/remove_all)+ ' -CHECKIMAGE_NAME '+strcompress(highz_path+'chk_'+image_root+num2write+'.fits',/remove_all)+' -MAG_ZEROPOINT '+string(zerop)+' -GAIN '+string(gain)+' -PIXEL_SCALE '+string(pixel_scale)+' -SEEING_FWHM '+string(seeing)+' -SATUR_LEVEL '+string(satur_level))
k=k+1

drop_field=field
drop_mask=mask


ENDWHILE
close,/all


IF tag_exist(cat_table,'FIELD_NUMBER') EQ 1 THEN BEGIN 
REMOVE_TAGS,cat_table,['FIELD_NUMBER','XCORD','YCORD','RASIMU','DECSIMU'],new_cat_table
cat_table=new_cat_table
ENDIF
cat_table=JJADD_TAG(cat_table,'FIELD_NUMBER',field_numberv)
cat_table=JJADD_TAG(cat_table,'XCORD',xposv)
cat_table=JJADD_TAG(cat_table,'YCORD',yposv)
cat_table=JJADD_TAG(cat_table,'RASIMU',rasimuv)
cat_table=JJADD_TAG(cat_table,'DECSIMU',decsimuv)



mwrfits,cat_table,strcompress(highz_path+dropped_catalog,/rem),/CREATE



FINAL_OUT:



END


;Copyright 2011 Marc Huertas-Company

;This file is part of GALSVM.

;    GALSVM is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 3 of the License, or
;    (at your option) any later version.

;    GALSVM is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.

 ;   You should have received a copy of the GNU General Public License
 ;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
