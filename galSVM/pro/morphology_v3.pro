function galSVM_gini,im,cxx,cyy,cxy

;+
;NAME:
;    galSVM_gini 
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;      gini= galSVM_gini (im, chk_im)
;
;DESCRIPTION :
; 
;Returns the gini coefficient of the image im 
; 
;
;  MANDATORY ARGUMENTS :
;    im             : (input) Galaxy image
;
;    chk_im         : (input) Segmentation image from Sextractor
;
;AUTHOR :
;    $Author: huertas-company $
;-


;Error handling
CATCH,error_status

IF error_status NE 0 THEN BEGIN
PRINT, 'Something went wrong while computing the Gini. Returning 99.9'
return,99.9
ENDIF

chk_im=galSVM_ellipse((size(im))[1],cxx,cyy,cxy,3.)  ;kron radius like ellipse

values=im(where(chk_im NE 0))
X=values(sort(values))
n=(size(X))(1)
x_mean=mean(X)
vec=2.*(findgen(n)+1)



gini=(1./(x_mean*n*(n-1)))*total((vec-n-1)*abs(X))

return, gini

end

function galSVM_smoothness,im,BG=im_b,kron_rad,a_image,cxx,cyy,cxy

;+
;NAME:
;    galSVM_smoothness 
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;      gini= galSVM_smoothness (im, im_chk, im_b, petro_rad, a_image, cxx, cyy, cxy)
;
;DESCRIPTION :
; 
;Returns the clumpiness of the galaxy im
; 
;
;  MANDATORY ARGUMENTS :
;    im             : (input) Galaxy image
;
;    chk_im         : (input) Segmentation image
;
;    im_b           : (input) SExtractor BACKGROUND parameter
;
;    petro_rad      : (input) Petrosian radius
;
;    a_image        : (input) SExtractor A_IMAGE parameter
;
;    cxx            : (input) SExtractor CXX parameter
;
;    cyy            : (input) SExtractor CYY parameter
;
;    cxy            : (input) SExtractor CXY paremeter
;
;AUTHOR :
;    $Author: huertas-company $
;-


;ERROR HANDLING...
CATCH,error_status

IF error_status NE 0 THEN BEGIN
PRINT, 'Something went wrong while computing the Smoothness. Returning 99.9'
return,99.9
ENDIF

im_s=smooth(im,kron_rad*a_image*0.15) ;I filter the image with a boxcar of 0.25*petro_rad
im_chk=galSVM_ellipse((size(im))[1],cxx,cyy,cxy,3.)
im_chk2=galSVM_ellipse((size(im))[1],cxx,cyy,cxy,1.)
annul=im_chk-im_chk2

im_sub=abs(im-im_s)

s_im=total(im_sub(where(annul NE 0.)))/total(abs(im(where(annul NE 0.))))

;same thing with the background...
IF KEYWORD_SET(im_b) THEN BEGIN
;print, 'enter'
imb_s=smooth(im_b,kron_rad*a_image*0.15)
imb_sub=abs(im_b-imb_s)
s_b=total(imb_sub(where(annul NE 0.)))/total(abs(im(where(annul NE 0.))))
ENDIF

;stop

IF KEYWORD_SET(im_b) THEN return,s_im-s_b ELSE return, s_im
END


function galSVM_m20,im,x_center,y_center, cxx, cyy, cxy

;+
;NAME:
;    galSVM_m20 - Calculates the m20 coefficient 
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;      m20= galSVM_m20 (im, im_chk, x_center, y_center)
;
;DESCRIPTION :
; 
;Returns the m20 coefficient of the galaxy im
; 
;
;  MANDATORY ARGUMENTS :
;    im             : (input) Galaxy image
;
;    im_chk         : (input) Segmentation image
;
;    x_center        : (input) X coordinate of the image barycenter
;
;    y_center        : (input) Y coordinate of the image barycenter
;
;
;AUTHOR :
;    $Author: huertas-company $
;-


;first M total

CATCH,error_status

IF error_status NE 0 THEN BEGIN
PRINT, 'Something went wrong while computing the M20. Returning 99.9'
return,99.9
ENDIF

;stop

im_size=(size(im))[1]

im_chk=galSVM_ellipse(im_size,cxx,cyy,cxy,3.) ; kron like ellipse

;seg_value=im_chk(x_center,y_center)

ftot=total(im(where(im_chk NE 0)))

flux_pixels_pos=where(im_chk NE 0)

mtot=0.


x_center_ini=x_center
y_center_ini=y_center



x_s=(indgen(20)-10)*.1
y_s=(indgen(20)-10)*.1

flag=0

for j=0.,(size(x_s))(1)-1 do begin
for k=0.,(size(x_s))(1)-1 do begin

x_center=x_center_ini+x_s(j)
y_center=y_center_ini+y_s(k)

for i=0.,(size(flux_pixels_pos))(1)-1 do begin
cord=array_indices(im,flux_pixels_pos(i))
factor=(cord(0)-x_center)*(cord(0)-x_center)+(cord(1)-y_center)*(cord(1)-y_center)
mtot=mtot+im(flux_pixels_pos(i))*factor
endfor

IF flag EQ 0. THEN BEGIN
flag=1
mtot_before=mtot
x_center_before=x_center
y_center_before=y_center
ENDIF

IF flag EQ 1 and mtot LT mtot_before THEN BEGIN 
mtot_before=mtot
x_center_before=x_center
y_center_before=y_center
ENDIF
endfor
endfor

mtot=mtot_before
x_center=x_center_before
y_center=y_center_before

flux_values=im(where(im_chk NE 0))
flux_values_sort=flux_values(sort(flux_values))
flux_values_sort=reverse(flux_values_sort)


mi=0.
i=0
sum_flux=0.
while sum_flux LT 0.2*ftot do begin
pos=where(im EQ flux_values_sort(i))
cord=array_indices(im,pos)
factor=(cord(0)-x_center)*(cord(0)-x_center)+(cord(1)-y_center)*(cord(1)-y_center)
mi=mi+flux_values_sort(i)*factor
sum_flux=sum_flux+flux_values_sort(i)
i=i+1
endwhile


;stop

return, alog10(mi/mtot)

END


function galSVM_petro_radius,gal,seg_im,seg_value,cxx,cyy,cxy,a_image,flux_annul,CIRCULAR=circular

;+
;NAME:
;galSVM_petro_radius 
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       petro=galSVM_petro_radius(gal,seg_im,seg_value,cxx,cyy,cxy,a_image,bg,flux_annul,/CIRCULAR)
;
;DESCRIPTION :
; 
;Returns the petrosian radius of the galaxy gal 
; 
;
;  MANDATORY ARGUMENTS :
;    gal            : (input) Galaxy image
;
;    seg_im         : (input) Segmentation image
;
;    seg_value      : (input) SExtractor NUMBER parameter
;
;    cxx            : (input) SExtractor cxx
;
;    cyy            : (input) SExtractor cyy
;
;    cxy            : (input) SExtractoor cxy
;
;    a_image        : (input) SExtractor a_image
;
;    flux_annul     : (output) Flux in the final annulus  
;  
;  OPTIONAL KEYWORDS:
;
;  CIRCULAR:         Set this keyword to obtain the petrosian using circular apertures instead of ellipses
;
;AUTHOR :
;    $Author: huertas-company $
;-



IF KEYWORD_SET(circular) THEN BEGIN
cxx=cyy
cxy=0.
ENDIF

factor=2
UP:

max_radius=((size(gal))[1]/2-2)*sqrt(cxx)
npoints=factor*max_radius/sqrt(cxx)

;npoints=100000./(size(gal))[1]

radius = findgen(npoints)/(npoints)*max_radius  ;I normalize the max radius to the image size
PRINT, "THE MAX RADIUS IS", max(radius)
PRINT, "THE NUMBER OF POINTS IS", npoints
flag=0



FOR i = 1, n_elements(radius)-1 DO BEGIN

;build an ellipse with increasing radius
ell=galSVM_ellipse((size(gal))(1),cxx,cyy,cxy,radius(i))
ell08=galSVM_ellipse((size(gal))(1),cxx,cyy,cxy,radius(i)*0.9)
ell125=galSVM_ellipse((size(gal))(1),cxx,cyy,cxy,radius(i)*1.1)

annulus=ell125-ell08

pos_gal=where(ell*gal NE 0. and (seg_im EQ 0. OR seg_im EQ seg_value), counts_gal)

;galaxy flux within the ellipse
flux_gal=total((ell*gal)(where((seg_im EQ 0. OR seg_im EQ seg_value))))/counts_gal


pos_annul=where(annulus*gal NE 0. and (seg_im EQ 0. OR seg_im EQ seg_value),counts_annul)
flux_annul=total((annulus*gal)[where((seg_im EQ 0. OR seg_im EQ seg_value))])/counts_annul

;stop

IF flux_gal*0.2 GT flux_annul THEN BEGIN
flag=1
GOTO, jump1
ENDIF
ENDFOR

;IF flag EQ 0 AND factor LT 50 THEN Begin
;PRINT, 'I AM JUMPING AGAING'
;factor=factor+5.
;GOTO,UP
;ENDIF



jump1:
stop
IF flag EQ 0. THEN RETURN,-1 ELSE RETURN,radius(i)

END



function galSVM_C_con,im,cxx,cyy,cxy,fmin,fmax,tot_flux

;+
;NAME:
;galSVM_C_con 
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       C=galSVM_C_con(im,x_center,y_center,osamp,fmin,fmax,tot_flux)
;
;DESCRIPTION :
; 
;Returns the concentration using the Conselice et al. formula
; 
;
;  MANDATORY ARGUMENTS :
;    im             : (input) Galaxy image
;
;    x_center       : (input) Center, X coordinate
;
;    y_center       : (input) Center, Y coordinate
;
;    osamp          : (input) Oversampling factor
;
;    fmin           : (output) Flux in the inner isophote 
;
;    fmax           : (output) Flux in the outer iophote
;
;    tot_flux       : (output) Total galaxy flux
;
;  
;
;
;AUTHOR :
;    $Author: huertas-company $
;-


;Error handling
CATCH,error_status

IF error_status NE 0 THEN BEGIN
PRINT, 'Something went wrong while computing the Concentration. Returning 99.9'
return,99.9
ENDIF

taille=(size(im))[1]


ell=galSVM_ellipse(taille,cxx,cyy,cxy,3.)
tot_flux=total(ell*im)


IF tot_flux LT 0. THEN BEGIN
return,99.9
ENDIF

rmin=0.
rmax=0.
j=0
fin=0
factor=.01

while fin LT tot_flux DO BEGIN
j=j+1
mask=galSVM_ellipse(taille,cxx,cyy,cxy,3*factor)
fin=total(mask*im)

IF fin GE fmin*tot_flux and rmin EQ 0. THEN rmin=3*factor

IF fin GE fmax*tot_flux and rmax EQ 0. THEN BEGIN 
rmax=3*factor
GOTO,OUT
ENDIF

factor=factor+.01

IF factor*3 GE 3. THEN RETURN,99.9

ENDWHILE

OUT:
print, 'RMAX:', rmax
print, 'RMIN:', rmin
return, 5*alog10(double(rmax)/rmin)

END

function galSVM_C_abr,im,cxx,cyy,cxy

;+
;NAME:
;galSVM_C_abr 
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       C=galSVM_C_abr(im,mxx,myy,mxy,osamp,r,rmax)
;
;DESCRIPTION :
; 
;Returns the concentration using the Abraham et al. formula
; 
;
;  MANDATORY ARGUMENTS :
;    im             : (input) Galaxy image
;
;    mxx            : (input) Second order moment 1
;
;    myy            : (input) Second order moment 2
;
;    mxy            : (input) Second order moment 3
;
;    osamp          : (input) Oversampling factor
;
;    r              : (input) Inner isophote radius 
;
;    rmax           : (input) Outer isophote radius
;
;      
;
;
;AUTHOR :
;    $Author: huertas-company $
;-


;Error handling
CATCH,error_status

IF error_status NE 0 THEN BEGIN
PRINT, 'Something went wrong while computing the Concentration. Returning 99.9'
return,99.9
ENDIF


ell=galSVM_ellipse((size(im))(1),cxx,cyy,cxy,3*0.3)
;im_over=congrid(im,(size(im))(1)*osamp,(size(im))(1)*osamp,/CENTER,CUBIC=-.5)/osamp^2
ell_total=galSVM_ellipse((size(im))(1),cxx,cyy,cxy,3.)

C=total(im[where(ell ne 0.)])/total(im[where(ell_total ne 0.)])


return,C

END



FUNCTION galSVM_asym, im,cxx,cyy,cxy,center_x,center_y,BG=BG, CONSELICE=CONSELICE

;+
;NAME:
;galSVM_asym 
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       A=galSVM_asym(im,im_chk,bg_value,center_x,center_y,FOND=fond,/CONSELICE)
;
;DESCRIPTION :
; 
;Returns the rotational asymmetry of the galaxy
; 
;
;  MANDATORY ARGUMENTS :
;    im             : (input) Galaxy image
;
;    im_chk         : (input) Segmentation image
;
;    bg_value       : (input) SExtractor BACKGROUND parameter
;
;    center_x       : (output) Galaxy center, X coordinate
;
;    center_y       : (output) Galaxy center, Y coordinate 
;
;  
;    KEYWORDS:
;
;    FOND: Set this keyword to subtract the backgrund
;
;    CONSELICE: Set this keyword to find the galaxy's center by minimizing the asymmetry
;      
;
;
;AUTHOR :
;    $Author: huertas-company $
;-


taille=(size(im))(1)

;Error handling
CATCH,error_status

IF error_status NE 0 THEN BEGIN
PRINT, 'Something went wrong while computing the Asymmetry. Returning 99.9'
return,99.9
ENDIF

;im=im-bg_value  ;substract the local background level

chk_big=galSVM_ellipse(taille,cxx,cyy,cxy,3.*1)
chk_small=galSVM_ellipse(taille,cxx,cyy,cxy,2.)
chk=chk_big-chk_small
chk=chk_big

im_ov=congrid(im,taille*5.,taille*5.,CUBIC=-.5)/5^2.
chk_ov=galSVM_ellipse(taille*5.,cxx,cyy,cxy,3.*5.)   ;kron like ellipse

center_x=round((size(im))[1]/2)
center_y=round((size(im))[2]/2)

;ell=galSVM_ellipse(taille,cxx,cyy,cxy,3)



IF KEYWORD_SET(conselice) THEN BEGIN ;the center is set to the one that minimizes the asymmetry


center_xi=center_x
center_yi=center_y

final_center_x=center_xi
final_center_y=center_yi

START:
ell=im*0
ell[final_center_x-2:final_center_x+3,final_center_y-2:final_center_y+3]=1

w=where(ell NE 0)
ind=array_indices(im,w)

flag=0

asymv=im*0

FOR i=0,(size(ind))[2]-1 DO BEGIN

center_x=ind[0,i]
center_y=ind[1,i]

im_rot=rot(im,180,1,center_x,center_y,/INTERP)

im_sub=im-im_rot

asym1=(total(abs(im_sub[where(chk ne 0.)])))/total(im[where(chk ne 0.)])


IF flag EQ 0 THEN BEGIN 
flag=1
asym_final=asym1
final_imsub=im_sub
ENDIF

IF abs(asym1) LT abs(asym_final) THEN BEGIN
asym_final=asym1 
final_center_x=center_x
final_center_y=center_y
final_imsub=im_sub
ENDIF

asymv[center_x,center_y]=asym1

endfor
;endfor

IF final_center_x NE center_xi OR final_center_y NE center_yi THEN BEGIN 
center_xi=final_center_x
center_yi=final_center_y
GOTO,START
ENDIF

center_x=final_center_x
center_y=final_center_y
im_sub=final_imsub






IF keyword_set(BG) THEN BEGIN
flag=0
final_center_x=center_xi
final_center_y=center_yi

START_BG:

ell=im*0
ell[final_center_x-2:final_center_x+3,final_center_y-2:final_center_y+3]=1


FOR i=0,(size(ind))[2]-1 DO BEGIN

center_xbg=ind[0,i]
center_ybg=ind[1,i]

bg_rot=rot(bg,180,1,center_xbg,center_ybg,/INTERP)

bg_sub=bg-bg_rot

asym1=(total(abs(bg_sub[where(chk ne 0.)])))/total(im[where(chk ne 0.)])


IF flag EQ 0 THEN BEGIN 
flag=1
asym_final_bg=asym1
ENDIF

IF abs(asym1) LT abs(asym_final) THEN BEGIN
asym_final_bg=asym1 
final_center_x=center_xbg
final_center_y=center_ybg
ENDIF



endfor
;endfor

IF final_center_x NE center_xi OR final_center_y NE center_yi THEN BEGIN 
center_xi=final_center_x
center_yi=final_center_y
GOTO,START_BG
ENDIF


ENDIF




;stop
IF KEYWORD_SET(BG) THEN asym_final=asym_final-asym_final_bg 
ENDIF



;stop

FIN:
RETURN, asym_final

END


function galSVM_r50,im,seg_im,xcenter,ycenter,osamp,rpetro
;+
;NAME:
;galSVM_r50 
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       ell=galSVM_ellipse(taille,a,b,c,r)
;
;DESCRIPTION :
; 
;Gives the radius containing 50% of the petrosian flux
; 
;
;  MANDATORY ARGUMENTS :
;    im             : (input) galaxy image
;
;    seg_im         : (input) galaxy segmentation map
;
;    xcenter         : (input) x coord. of galaxy
;
;    ycenter          : (input) y coord. of galaxy
;
;    osamp             : (input) oversampling factor
;
;    rpetro           : (input) petrosian radius
;
;  
;
;   
;AUTHOR :
;    $Author: huertas-company $
;-


size_im=(size(im))[1]
imos=congrid(im,size_im*osamp,size_im*osamp,CUBIC=-.5)/osamp^2.
mask=shift(dist(size_im*osamp),xcenter*osamp,ycenter*osamp) LT rpetro
tot_petro_flux=total(im[where(mask NE 0 AND seg_im EQ 0)])

WHILE inner_flux LT 0.5*tot_petro DO BEGIN

maskinn=shift(dist(size_im*osamp),xcenter*osamp,ycenter*osamp) LT ri
inner_flux=total(im[where(maskinn NE 0 AND seg_im EQ 0)])
ri=ri+1
;just in case it does not work
IF ri GE rpetro THEN GOTO, OUT
ENDWHILE

OUT:
RETURN, ri

END


function galSVM_ellipse, taille,a,b,c,r

;+
;NAME:
;galSVM_ellipse 
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       ell=galSVM_ellipse(taille,a,b,c,r)
;
;DESCRIPTION :
; 
;Builds an ellipse
; 
;
;  MANDATORY ARGUMENTS :
;    taille         : (input) Size of the output image
;
;    a              : (input) A parameter
;
;    b              : (input) B parameter
;
;    c              : (input) C parameter
;
;    r              : (input) Radius 
;
;  
;
;   
;AUTHOR :
;    $Author: huertas-company $
;-


ON_ERROR,2
im2=fltarr(taille*1.,taille*1.)
cord=im2

for i=0,taille*1.-1 do begin
cord(i,*)=i*(1/1.)-taille/2
endfor

im2=cord^2*a+transpose(cord)^2*b+c*cord*transpose(cord)

cord(*,*)=0.
cord(where(im2 le r*r))=1.
cord=rebin(cord,taille,taille)

return, cord

end



function galSVM_build_seg_map,gal,chk,seg_value

;+
;NAME:
;galSVM_build_seg_map 
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       seg_map=galSVM_build_seg_map(gal,chk,seg_value)
;
;DESCRIPTION :
; 
;Builds an ellipse
; 
;
;  MANDATORY ARGUMENTS :
;    gal            : (input) Galaxy image
;
;    chk_seg        : (input) SExtractor segmentation image
;
;    seg_value      : (input) SExtractor number
;
;
;
;  
;
;   
;AUTHOR :
;    $Author: huertas-company $
;-


; I dilate the segmentation image
chk=ulong(chk)
chk=mm_dil_n(chk,3)

;I remove the foreground stars or galaxies
bg_level=median(gal[where(chk EQ 0.)])
bg_sig=sqrt(variance(gal[where(chk EQ 0.)]))
rand_noise=randomn(seed,(size(gal))[1],(size(gal))[1])*bg_sig+bg_level

pos_other=where(chk NE seg_value and chk NE 0.)
IF pos_other(0) NE -1 THEN gal(pos_other)=rand_noise(pos_other)  ;remove the other sources in the image

;Convolve the image

seg_map=gal*0.
match=where(chk EQ seg_value)
IF match[0] NE -1 THEN seg_map(where(chk EQ seg_value))=1.


RETURN,seg_map

END



function galSVM_morphology, im,seg_im,seg_value,seg_map,bg_value,kron_rad,a_image,zeropoint,pix_size,fond,ELLIPSE=ellipse

;+
;NAME:
;    galSVM_morphology
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       morpho=galSVM_morphology(im, seg_im, seg_value, seg_map, bg_value, petro_rad, a_image, zeropoint, $
;                                pix_size, FOND=fond, ELLIPSE=ellipse)
;
;DESCRIPTION :
; 
;Returns the main morphological parameters measured on a galaxy stamp
; 
;
;  MANDATORY ARGUMENTS :
;    im             : (input) Stamp image of the galaxy
;
;    seg_im         : (input) SExtractor segmentation map
;
;    seg_value      : (input) SExtractor NUMBER parameter 
;
;    seg_map        : (input) Another segmentation map built from the dilated image
;
;    bg_value       : (input) SExtractor BACKGROUND parameter
;
;    petro_rad      : (input) Petrosian radius
;
;    a_image        : (input) SExtractor A_IMAGE parameter
;
;    zeropoint      : (input) Image zeropoint
;
;    pix_size       : (input) Image pixel size in arcsec/pixel
;
;OPTIONAL KEYWORDS:    
;
;AUTHOR :
;    $Author: huertas-company $
;-


;this keyword needs to be set
IF keyword_set(ELLIPSE) THEN BEGIN
cxx=ellipse[0]
cyy=ellipse[1]
cxy=ellipse[2]
ENDIF





;;****************************
;;SURFACE BRIGHTNESS
;;*****************************

osamp=2.
im_chk=galSVM_ellipse((size(im))(1),cxx,cyy,cxy,3)
mu=total(im[where(im_chk ne 0.)])/(total(im_chk(where(im_chk ne 0.)))/mean(im_chk(where(im_chk ne 0.))))
mu=zeropoint-2.5*alog10(mu/pix_size^2)  ;;in mag/arcsec2


;;*************************
;; ASYMMETRY
;;*************************

asym_final=galSVM_asym(im-bg_value,cxx,cyy,cxy,center_x,center_y,BG=fond-bg_value,/CON)


;**************************************
;******CENTRAL CONCENTRATION********************
;************************************

;IF KEYWORD_SET(ELLIPSE) THEN C3=galSVM_C_abr(im,mxxb,myyb,mxyb,1.,.3*3.,3.) ELSE C3=galSVM_C_abr(im,mxxb,myyb,mxyb,.3) 

C3=galSVM_C_abr(im,cxx,cyy,cxy)


;prod=im*im_chk ;flux contained in 1.5*rp
;IF (where(prod NE 0 and (seg_im EQ 0. or seg_im EQ seg_value)))(0) EQ -1 THEN BEGIN
;;something is really wrong...
;abr=[99.9,99.9,99.,99.9,99.9,99.9,99.9]
;return,abr
;ENDIF


;tot_flux=total(prod(where(prod NE 0)))

;Ccon=galSVM_C_con(im,center_x,center_y,2.,0.2,0.8,tot_flux)

Ccon=galSVM_C_con(im,cxx,cyy,cxy,0.2,0.8,tot_flux)


;***************************************
;*********GINI COEFFICIENT**************
;***************************************

g=galSVM_gini(im,cxx,cyy,cxy)


;***************************************
;***********SMOOTHNESS*****************
;**************************************
;stop
;print, 'A IMAGE',a_image
s=galSVM_smoothness(im-bg_value,BG=fond-bg_value,kron_rad,a_image,cxx,cyy,cxy)


;***************************************
;*************M20**********************
;**************************************

m=galSVM_m20(im,center_x,center_y,cxx,cyy,cxy)



IF keyword_set(ELLIPSE) THEN GOTO,FIN

abr=fltarr(7)
abr=[mu,abs(asym_final),mxxb,myyb,mxyb,C3,g,r]


error:
return, abr


FIN:
abr=fltarr(6)
abr=[mu,asym_final,C3,g,s,m,Ccon]
print, 'CONCENTRATION (ABRAHAM): ', C3
print, 'CONCENTRATION (BERSHADY-CONSELICE): ', Ccon
print, 'ASYMMETRY: ', asym_final
print, 'GINI: ', g
print, 'SMOOTHNESS: ',s
print, 'M20: ', m
return,abr


end


pro galSVM_mock_morpho_catalog,highz_path,im_name,par_file,pix_size,zeropoint,catalog_name,SHOW=show,DIMMING=DIMMING

;+
;NAME:
;galSVM_MOCK_MORPHO_CATALOG 
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       galSVM_mock_morpho_catalog, im_name, par_file, pix_size, zeropoint, catalog_name
;
;DESCRIPTION :
; 
;Returns a catalog with some morphological parameters measured on the objects 
; 
;
;  MANDATORY ARGUMENTS :
;    im_name        : (input) Name of the images
;
;    par_file       : (input) Catalogue with the objects to be analyzed
;
;    pix_size       : (input) Pixel size in arcsec/pixel
;
;    zeropoint      : (input) Image zeropoint
;
;    catalogue_name : (output) Name of the output catalogue  
;
;  KEYWORDS:
;
;   SHOW            : shows a stamps of each analyzed galaxy 
;
;AUTHOR :
;    $Author: huertas-company $
;-


cd, highz_path

un=1
k=0
last_j=0
m=0


table_param=mrdfits(par_file,1)
fields=table_param.FIELD_NUMBER
first=floor(min(fields))
last=floor(max(fields))
size_x=(size(fields))[1]
size_y=n_tags(table_param)



field_before=0
ngal=0


;intialize vectors containing measured parameters
con_vector=fltarr(size_x)+99.9
mu_vector=con_vector+99.9
as_vector=con_vector+99.9
gini_vector=con_vector+99.9
m20_vector=con_vector+99.9
smooth_vector=con_vector+99.9
con2_vector=con_vector+99.9

gal_numberV=fltarr(size_x)
bg_valueV=fltarr(size_x)
magV=fltarr(size_x)
petro_radV=fltarr(size_x)
kron_radV=fltarr(size_x)
a_imageV=fltarr(size_x)
b_imageV=fltarr(size_x)
theta_imageV=fltarr(size_x)
elonV=fltarr(size_x)
ellV=fltarr(size_x)
cxxV=fltarr(size_x)
cyyV=fltarr(size_x)
cxyV=fltarr(size_x)
rhV=fltarr(size_x)
isoaV=fltarr(size_x)


for i=0,size_x-1 do begin

field=floor((table_param.FIELD_NUMBER)[i])

IF field NE field_before and field NE 0. THEN BEGIN
field_before=field
number2read=string(field,FOR='(I05)')
im=readfits(strcompress(im_name+number2read+'.fits',/remove_all))
chk=readfits(strcompress(strcompress('chk_'+im_name,/remove_all)+number2read+'.fits',/remove_all))
table=mrdfits(strcompress(im_name+number2read+'.fit',/remove_all),2)
size_x_table=(size(table.NUMBER))[1]

ENDIF


 imbis=im


pos= galSVM_select_simu_galaxies(table,table_param,i)

IF pos NE -1 THEN BEGIN

x=(table.X_IMAGE)[pos]
y=(table.Y_IMAGE)[pos]
number=(table.NUMBER)[pos]


ngal=ngal+1
PRINT, 'GALAXY NUMBER: ',i
gal_number=(table.NUMBER)[pos]
bg_value=(table.BACKGROUND)[pos]
mag=(table.MAG_AUTO)[pos]
petro_rad=(table.PETRO_RADIUS)[pos]
kron_rad=(table.KRON_RADIUS)[pos]
a_image=(table.A_IMAGE)[pos]
b_image=(table.B_IMAGE)[pos]
theta_image=(table.THETA_IMAGE)[pos]
elon=(table.ELONGATION)[pos]
ell=(table.ELLIPTICITY)[pos]
cxx=(table.CXX_IMAGE)[pos]
cyy=(table.CYY_IMAGE)[pos]
cxy=(table.CXY_IMAGE)[pos]
rh=(table.FLUX_RADIUS)[pos]
isoa=(table.ISOAREA_IMAGE)[pos]

gal_numberV[i]=gal_number
bg_valueV[i]=(table.BACKGROUND)[pos]
magV[i]=(table.MAG_AUTO)[pos]
petro_radV[i]=(table.PETRO_RADIUS)[pos]
kron_radV[i]=(table.KRON_RADIUS)[pos]
a_imageV[i]=(table.A_IMAGE)[pos]
b_imageV[i]=(table.B_IMAGE)[pos]
theta_imageV[i]=(table.THETA_IMAGE)[pos]
elonV[i]=(table.ELONGATION)[pos]
ellV[i]=(table.ELLIPTICITY)[pos]
cxxV[i]=(table.CXX_IMAGE)[pos]
cyyV[i]=(table.CYY_IMAGE)[pos]
cxyV[i]=(table.CXY_IMAGE)[pos]
rhV[i]=(table.FLUX_RADIUS)[pos]
isoaV[i]=(table.ISOAREA_IMAGE)[pos]

sigma_bg=sqrt(variance(im(where(chk EQ 0. ))))
mean_bg=mean(im(where(chk EQ 0.)))

petro_sex=petro_rad
bad_petro=0

flag=0
factor=1.5
SIZE_DEF:
size=round(factor*kron_rad*a_image)  ;the stamp size is 3 times the kron radius
IF size EQ 0 THEN size=factor*sqrt(isoa)  ;sometimes the kron radius is 0



IF x+size/2 ge (size(im))[1] OR x-size/2 LT 0 OR  y+size/2 ge (size(im))[2] OR y-size/2 LT 0 OR size LE 0. THEN BEGIN
GOTO,TOO_CLOSE
ENDIF


IF KEYWORD_SET(DIMMING) THEN GOTO, TOO_CLOSE

gal=im[x-size/2:x+size/2-1,y-size/2:y+size/2-1]
gal_chk=chk[x-size/2:x+size/2-1,y-size/2:y+size/2-1]


ELLIPSE:
petro_ell=galSVM_ellipse((size(gal))(1),cxx,cyy,cxy,3.)

;;BUILD THE SEGMENTATION MAP

seg_map=galSVM_build_seg_map(gal,gal_chk,gal_number)

;procedure for selecting a background region...

a=0
k=10
flag=0
no_fond=0
while a ne -1 do begin
if x+k+size-1 lt (size(im))[1] and flag eq 0 then begin
fond=im[x+k:x+k+size-1,y-size/2:y+size/2-1]
fd_chk=chk[x+k:x+k+size-1,y-size/2:y+size/2-1]
endif else begin
if flag eq 0 then begin 
k=10
flag=1
endif
IF y+k+size-1 lt (size(im))[2] and flag eq 1 then begin
fond=im[x-size/2:x+size/2-1,y+k:y+k+size-1]
fd_chk=chk[x-size/2:x+size/2-1,y+k:y+k+size-1]
ENDIF ELSE BEGIN
IF flag EQ 1 THEN BEGIN 
k=-10 
flag=2 
ENDIF
if x+k-size-1 ge 0 and flag eq 2 then begin
fond=im[x+k-size-1:x+k,y-size/2:y+size/2-1]
fd_chk=chk[x+k-size-1:x+k,y-size/2:y+size/2-1]
ENDIF else begin
if flag eq 2 then begin 
k=-10
flag=3
endif

IF y+k-size-1 ge 0 and flag eq 3 then begin
fond=im[x-size/2:x+size/2-1,y+k-size-1:y+k]
fd_chk=chk[x-size/2:x+size/2-1,y+k-size-1:y+k]
ENDIF ELSE BEGIN
PRINT, "CREATING ARTIFICAL BACKGROUND!"
fond=randomn(-64362,size,size)*sigma_bg +mean_bg; I create an artificial background
GOTO,OUT_BUCLE
ENDELSE
ENDELSE

ENDELSE

endelse


IF no_fond EQ 1 THEN a =-1 ELSE a=(where(fd_chk*petro_ell ne 0))[0]
IF K GT 0. THEN k=k+5 ELSE k=k-5.

endwhile


OUT_BUCLE:
;*******MORPHOLOGY ESTIMATORS**********************************

;;;;;;;;;; concentration, gini, asymetry....

abr=galSVM_morphology(gal,gal_chk,gal_number,seg_map,bg_value,kron_rad,a_image,zeropoint,pix_size,fond,ELLIPSE=[cxx,cyy,cxy])


mu_vector[i]=abr[0]
as_vector[i]=abr[1]
con_vector[i]=abr[2]
gini_vector[i]=abr[3]
smooth_vector[i]=abr[4]
m20_vector[i]=abr[5]
con2_vector[i]=abr[6]





ENDIF ELSE BEGIN 

TOO_CLOSE:

;objects too close to the image border are set to -100
mu_vector[i]=-100
as_vector[i]=-100
con_vector[i]=-100
gini_vector[i]=-100
smooth_vector[i]=-100
m20_vector[i]=-100
con2_vector[i]=-100


ENDELSE






;endfor



endfor


IF tag_exist(table_param,'MUMEAN') EQ 1 THEN BEGIN 
REMOVE_TAGS,table_param,['NUMBER','MAG_AUTO','PETRO_RADIUS','FLUX_RADIUS','ISOAREA_IMAGE','ELONGATION','ELLIPTICITY','MUMEAN','ASYM','CABR','GINI','SMOOTH','M20','CCON'],new_table
table_param=new_table
print, 'Existing parameters from previous runs have been removed!'
ENDIF



table_param=JJADD_TAG(table_param,'NUMBER',gal_numberV)
table_param=JJADD_TAG(table_param,'MAG_AUTO',magV)
table_param=JJADD_TAG(table_param,'PETRO_RADIUS',petro_radV)
table_param=JJADD_TAG(table_param,'FLUX_RADIUS',rhV)
table_param=JJADD_TAG(table_param,'ISOAREA_IMAGE',isoaV)
table_param=JJADD_TAG(table_param,'ELONGATION',elonV)
table_param=JJADD_TAG(table_param,'ELLIPTICITY',ellV)



table_param=JJADD_TAG(table_param,'MUMEAN',mu_vector)
table_param=JJADD_TAG(table_param,'ASYM',as_vector)
table_param=JJADD_TAG(table_param,'CABR',con_vector)
table_param=JJADD_TAG(table_param,'GINI',gini_vector)
table_param=JJADD_TAG(table_param,'SMOOTH',smooth_vector)
table_param=JJADD_TAG(table_param,'M20',m20_vector)
table_param=JJADD_TAG(table_param,'CCON',con2_vector)

mwrfits,table_param,strcompress(catalog_name,/rem),/CREATE



FINAL_OUT:


END

FUNCTION galSVM_select_simu_galaxies,sex_table,par_table,pos

;+
;NAME:
;    galSVM_select_simu_galaxies
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       galSVM_select_simu_galaxies, sex_file_name, par_file
;
;DESCRIPTION :
; 
;Selects the simulated galaxies from the SExtractor ouput and creates new catalogues with the extension "_sel"
; 
;
;  MANDATORY ARGUMENTS :
;    sex_table        : (input) sextractor's strucutre
;
;    par_table      : (input) structure with the objects to be analyzed
;
;    pos          : (input) position to look for
;
;
;
;
;AUTHOR :
;    $Author: huertas-company $
;-





size_y=n_tags(par_table)

field=(par_table.FIELD_NUMBER)[pos]
x=(par_table.XCORD)[pos]
y=(par_table.YCORD)[pos]


n=(size(sex_table.NUMBER))[1]


match=(where(sex_table.X_IMAGE ge x-10 and sex_table.X_IMAGE le x+10 and sex_table.Y_IMAGE ge y-10 and sex_table.Y_IMAGE le y+10))[0]


RETURN, match


END

pro galSVM_real_morpho_catalog,table,im,positions,chk,pix_size,zeropoint,output_path,file_out

;+
;NAME:
;    galSVM_real_morpho_catalog
;
;CATEGORY :
;       Astronomy
;
;SYNTAX :
;       galSVM_real_morpho_catalog, cat_filename, im, chk, pix_size, zeropoint,file_out
;
;DESCRIPTION :
; 
;Computes morphological parameters on the real catalogue
; 
;
;  MANDATORY ARGUMENTS :
;    table          : (input) SExtractor catalogue
;
;    im             : (input) Input image
;
;    positions       : (input) Positions of objects to be analized. all other objects wil be ignored
;
;    chk            : (input) SExtractor segmentation map
;
;    pix_size       : (input) Pixel scale length
;
;    zeropoint      : (input) Zeropoint of im  
;
;    output_path    : (input) Directory where the output catalog will be saved   
;
;    file_out       : (output) Output catalog name
;
;AUTHOR :
;    $Author: huertas-company $
;-



nobjects=(size(table.NUMBER))[1]

print, "NOBJECTS: ", nobjects

FLAG_TABLE=0

xv=table.X_IMAGE
yv=table.Y_IMAGE
gal_numberv=table.NUMBER
bg_valuev=table.BACKGROUND
magv=table.MAG_AUTO
petro_radv=table.PETRO_RADIUS
kron_radV=table.KRON_RADIUS
a_imagev=table.A_IMAGE
b_imagev=table.B_IMAGE
theta_imagev=table.THETA_IMAGE
elonv=table.ELONGATION
ellv=table.ELLIPTICITY
cxxv=table.CXX_IMAGE
cyyv=table.CYY_IMAGE
cxyv=table.CXY_IMAGE
rhv=table.FLUX_RADIUS
isoav=table.ISOAREA_IMAGE

flag2close=0

num=0
for i=0.,nobjects-1 do begin

match_pos=where(positions EQ i)

IF match_pos[0] NE -1 THEN BEGIN 
print, 'OBJECT N: '+ string(table[i].NUMBER)
num=num+1
;getting parameters from the SExtractor catalogue

x=xv[i]
y=yv[i]
gal_number=gal_numberv[i]
bg_value=bg_valuev[i]
mag=magv[i]
petro_rad=petro_radv[i]
kron_rad=kron_radv[i]
a_image=a_imagev[i]
b_image=b_imagev[i]
theta_image=theta_imagev[i]
elon=elonv[i]
ell=ellv[i]
cxx=cxxv[i]
cyy=cyyv[i]
cxy=cxyv[i]
rh=rhv[i]
isoa=isoav[i]



bad_petro=0

flag=0
factor=1.5
petro_sex=petro_rad
SIZE_DEF:


size=round(factor*kron_rad*a_image)  ;the stamp size is 4 times the petrosian radius
IF size EQ 0 THEN size=factor*sqrt(isoa)

IF x+size/2 ge (size(im))[1] OR x-size/2 LT 0 OR  y+size/2 ge (size(im))[2] OR y-size/2 LT 0 OR size EQ 0. THEN BEGIN
print, "Object too close to borders"
flag2close=1
GOTO,TOO_CLOSE
ENDIF


gal=im[x-size/2:x+size/2-1,y-size/2:y+size/2-1]
gal_chk=chk[x-size/2:x+size/2-1,y-size/2:y+size/2-1]
petro_ell=galSVM_ellipse((size(gal))[1],cxx,cyy,cxy/(-2),3.)



ELLIPSE:
;petro_ell=galSVM_ellipse((size(gal))(1),cyy,cxx,cxy/(-2),1.5*petro_rad)

;;BUILD THE SEGMENTATION MAP

;seg_map=galSVM_build_seg_map(gal,gal_chk,gal_number)

a=0
k=10
flag=0

while a ne -1 do begin
if x+k+size-1 lt (size(im))[1] and flag eq 0 then begin
fond=im[x+k:x+k+size-1,y-size/2:y+size/2-1]
fd_chk=chk[x+k:x+k+size-1,y-size/2:y+size/2-1]
endif else begin
if flag eq 0 then begin 
k=10
flag=1
endif
IF y+k+size-1 lt (size(im))[2] and flag eq 1 then begin
fond=im[x-size/2:x+size/2-1,y+k:y+k+size-1]
fd_chk=chk[x-size/2:x+size/2-1,y+k:y+k+size-1]
ENDIF ELSE BEGIN
IF flag EQ 1 THEN BEGIN 
k=-10 
flag=2 
ENDIF
if x+k-size-1 ge 0 and flag eq 2 then begin
fond=im[x+k-size-1:x+k,y-size/2:y+size/2-1]
fd_chk=chk[x+k-size-1:x+k,y-size/2:y+size/2-1]
ENDIF else begin
if flag eq 2 then begin 
k=-10
flag=3
endif

IF y+k-size-1 ge 0 and flag eq 3 then begin
fond=im[x-size/2:x+size/2-1,y+k-size-1:y+k]
fd_chk=chk[x-size/2:x+size/2-1,y+k-size-1:y+k]
ENDIF ELSE BEGIN 
PRINT, "CREATING ARTIFICAL BACKGROUND!"
IF (gal(where(gal_chk EQ 0.)))(0) EQ -1 THEN GOTO,TOO_CLOSE
sigma_bg=sqrt(variance(gal(where(gal_chk EQ 0. ))))
mean_bg=mean(gal(where(gal_chk EQ 0.)))
fond=(randomn(-64362,size,size)+bg_value)*sigma_bg ; I create an artificial background
GOTO,OUT_BUCLE
ENDELSE
ENDELSE

ENDELSE

endelse


a=(where(fd_chk*petro_ell ne 0))[0]
IF K GT 0. THEN k=k+5 ELSE k=k-5.


endwhile

OUT_BUCLE:

;MORPHOLOGY...


mor=galSVM_morphology(gal,gal_chk,gal_number,seg_map, bg_value,kron_rad,a_image,zeropoint,pix_size,fond,ELLIPSE=[cxx,cyy,cxy])

IF FLAG_TABLE EQ 0 THEN BEGIN  ;initialization
FLAG_TABLE=1

muv=ellv*0.
Cabrv=muv
asymv=muv
giniv=muv
smoothv=muv
m20v=muv
Cconv=muv

ENDIF

muv[i]=mor[0]
Cabrv[i]=mor[2]
asymv[i]=mor[1]
giniv[i]=mor[3]
smoothv[i]=mor[4]
m20v[i]=mor[5]
Cconv[i]=mor[6]

ENDIF ELSE BEGIN



IF FLAG_TABLE EQ 0 THEN BEGIN  ;initialization
FLAG_TABLE=1
muv=ellv*0.
Cabrv=muv
asymv=muv
giniv=muv
smoothv=muv
m20v=muv
Cconv=muv
ENDIF

muv[i]=-99.9
Cabrv[i]=-99.9
asymv[i]=-99.9
giniv[i]=-99.9
smoothv[i]=-99.9
m20v[i]=-99.9
Cconv[i]=-99.9

ENDELSE


TOO_CLOSE:

IF FLAG_TABLE EQ 0 THEN BEGIN  ;initialization
FLAG_TABLE=1
muv=ellv*0.
Cabrv=muv
asymv=muv
giniv=muv
smoothv=muv
m20v=muv
Cconv=muv
ENDIF

IF flag2close EQ 1 THEN BEGIN
muv[i]=-100
Cabrv[i]=-100
asymv[i]=-100
giniv[i]=-100
smoothv[i]=-100
m20v[i]=-100
Cconv[i]=-100
flag2close=0
ENDIF

ENDFOR


IF tag_exist(table,'MUMEAN') EQ 1 THEN BEGIN 
REMOVE_TAGS,table,['MUMEAN','ASYM','CABR','GINI','SMOOTH','M20','CCON'],new_table
table=new_table
ENDIF

table=JJADD_TAG(table,'MUMEAN',muv)
table=JJADD_TAG(table,'ASYM',asymv)
table=JJADD_TAG(table,'CABR',Cabrv)
table=JJADD_TAG(table,'GINI',giniv)
table=JJADD_TAG(table,'SMOOTH',smoothv)
table=JJADD_TAG(table,'M20',m20v)
table=JJADD_TAG(table,'CCON',Cconv)
table.PETRO_RADIUS=petro_radV

cd, output_path
mwrfits,table,strcompress(file_out,/rem),/CREATE


FINAL_OUT:



END



;Copyright 2011 Marc Huertas-Company

;This file is part of GALSVM.

;    GALSVM is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 3 of the License, or
;    (at your option) any later version.

;    GALSVM is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.

 ;   You should have received a copy of the GNU General Public License
 ;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
