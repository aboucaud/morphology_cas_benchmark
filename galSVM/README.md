galSVM
======

Installation
------------

* Add the path the path to the code to the `IDL_PATH` variable.
* Make sure it runs by opening an IDL prompt and running
```
.r make_galsvm
```

Requirements
------------

* `SExtractor` > 2.5.0

Set Up
------

* Create a folder containing the following directories
    * tiles
    * cat
    * seg
    * morpho
    * galsvm
* Place the data (FITS images) in the `tiles/` directory.
* Pull all the config files from this repository and place
  `mag_z.txt` & `z_histo.txt` under `cat/` and the remaining files
  (`sextractor_config.sex`, `LOCAL_CAT_forGALSVM_allNair2.fit`, `default.nnw`,
  `galSVM.config`, `gauss_4.0_7x7.conv`, `se_param_full.param`) under
  `galsvm/`.
* Change the paths in both `galSVM.config` and `sextractor_config.sex`
  according to your own system.

SExtractor run
--------------

* From the `tiles/` directory, assuming the data is the `image.fits` file, run
```
sex -c ../galsvm/sextractor_config.sex image.fits -CATALOG_NAME ../cat/image_cat.fits -CHECKIMAGE_NAME ../seg/image_seg.fits
```

* This should create both a catalog in `cat/` and a segmentation map in `seg/`.

galSVM run
----------

* From the `galsvm/` directory, open an IDL console and run
```
.r make_galsvm
galsvm "morphology_real"
```

* This should create an updated catalog in `morpho/` with the morphological measurements.
